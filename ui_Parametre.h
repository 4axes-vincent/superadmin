/********************************************************************************
** Form generated from reading UI file 'Parametre.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARAMETRE_H
#define UI_PARAMETRE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Parametre
{
public:

    void setupUi(QWidget *Parametre)
    {
        if (Parametre->objectName().isEmpty())
            Parametre->setObjectName(QString::fromUtf8("Parametre"));
        Parametre->resize(806, 617);

        retranslateUi(Parametre);

        QMetaObject::connectSlotsByName(Parametre);
    } // setupUi

    void retranslateUi(QWidget *Parametre)
    {
        Parametre->setWindowTitle(QApplication::translate("Parametre", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Parametre: public Ui_Parametre {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARAMETRE_H
