#include "Ajouter_VM.h"
#include "ui_Ajouter_VM.h"

Ajouter_VM::Ajouter_VM(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Ajouter_VM){

    ui->setupUi(this);
    initialiser();
}




//----------------------------------------------------------------------------------------------------------------------
Ajouter_VM::~Ajouter_VM(){

    delete ui;
}



//----------------------------------------------------------------------------------------------------------------------
void Ajouter_VM::initialiser(){

    this->setWindowTitle("Ajouter une nouvelle VM");
    this->setFixedSize(317,210);

}



//----------------------------------------------------------------------------------------------------------------------
void Ajouter_VM::add_VM(){

    //REGEX @IP
    QRegExp reg("^\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b$");

    QString numVM           = ui->lineEdit_numVM->text();
    QString adrIP           = ui->lineEdit_IP->text();

    //Tester l'existence des informations
    if( numVM.isEmpty() && adrIP.isEmpty() )
        QMessageBox::warning(NULL,"Informations formulaire","Veuillez remplir tous les deux champs !");
    else{

        //Tester si @IP format correcte
        if( ! adrIP.contains(reg) )
            QMessageBox::warning(NULL,"Informations formulaire","Indiquer une adresse IP correcte !");
        else{

            // Cr�ation du fichier en pr�cisant que l'on travaille avec un fichier de format INI.
            QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);

            // Cr�ation du groupe [VM]
            settings.beginGroup("VM");

            //On ajoute l'information (numVM = adrIP)
            settings.setValue("4AXES_"+numVM, adrIP);

            QMessageBox::information(NULL,"Informations ajout�es","Les informations ont bien �t� ajout�e !");

            close();
        }
    }
}




//----------------------------------------------------------------------------------------------------------------------
void Ajouter_VM::on_btn_annuler_clicked(){

    close();
}




//----------------------------------------------------------------------------------------------------------------------
void Ajouter_VM::on_btn_valider_clicked(){

    add_VM();
}
