#include "Trieur.h"

Trieur::Trieur(QString dir_GOOD, QString dir_UNKNOWN){

    pathG = dir_GOOD;
    pathU = dir_UNKNOWN;
}


//----------------------------------------------------------------------------------------------------------------------
QStringList Trieur::trierErreur (QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryList;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);


    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext () ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;
        dossier = info.absoluteFilePath () + "/ERREUR";
        entryList = dossier.entryInfoList (QDir::Files | QDir::NoDotAndDotDot);
        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        nbGood = entryList.length ();

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList (QDir::Files);
        nbUnknown = entryList.length ();


        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);
}










QStringList Trieur::trierErreurDate (QDate date, QLineEdit *IHM_Ligne_Total){


    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::AllEntries | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;

        dossier = info.absoluteFilePath () + "\\ERREUR";
        entryListInfo = dossier.entryInfoList (QDir::Files);

        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        if( !entryListInfo.isEmpty () ){

            for(int i = 0 ; i < entryListInfo.length () ; i++){

                if( entryListInfo.at (i).created ().toString ("yyyyMMdd") == date.toString ("yyyyMMdd"))
                    nbGood++;
            }
        }


        dossier = pathU + "\\" + mutuelle;
        entryListInfo = dossier.entryInfoList (QDir::Files);

        if( !entryListInfo.isEmpty () ){

            for(int i = 0 ; i < entryListInfo.length () ; i++){

                if( entryListInfo.at (i).created ().toString ("yyyyMMdd") == date.toString ("yyyyMMdd"))
                    nbUnknown++;
            }
        }

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }

    }
    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);
}







QStringList Trieur::trierErreurPeriode (QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total){


    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;

    QDir dossier;
    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

            nbGood = 0;
            nbUnknown = 0;

            info = dirOC.next () ;

            dossier = info.absoluteFilePath () + "\\ERREUR";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            path = info.absoluteFilePath ();
            mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbGood++;
                }
            }

            dossier = pathU + "\\" + mutuelle;
            entryListInfo = dossier.entryInfoList (QDir::Files);

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbUnknown++;
                }
            }

            if(nbGood+nbUnknown != 0){

                total += nbGood + nbUnknown;
                listStats.push_back (mutuelle);
                listStats.push_back (QString::number (nbGood));
                listStats.push_back (QString::number (nbUnknown));
                listStats.push_back (QString::number (nbGood+nbUnknown));
            }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}
















//----------------------------------------------------------------------------------------------------------------------
QStringList Trieur::trierOK_MANUEL (QLineEdit *IHM_Ligne_Total){


    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryList;
    QDir dossier;

    QFileInfo info;


    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;

        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
        entryList = dossier.entryInfoList (QDir::Files);

        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        nbGood = entryList.length ();

        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
        entryList = dossier.entryInfoList (QDir::Files);
        nbUnknown = entryList.length ();

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);
}









QStringList Trieur::trierOK_MANUELDate (QDate date, QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::AllEntries | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;

        dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL\\" % date.toString ("yyyyMMdd");
        entryListInfo = dossier.entryInfoList (QDir::Files);

        nbGood = entryListInfo.length ();

        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL\\" % date.toString ("yyyyMMdd");
        entryListInfo = dossier.entryInfoList (QDir::Files);

        nbUnknown = entryListInfo.length ();

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }

    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}














QStringList Trieur::trierOK_MANUELPeriode (QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total){


    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;

    QDir dossier;
    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

            nbGood = 0;
            nbUnknown = 0;

            info = dirOC.next () ;

            dossier = info.absoluteFilePath () + "\\ERREUR\\OK_MANUEL";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            path = info.absoluteFilePath ();
            mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbGood++;
                }
            }

            dossier = pathU + "\\" + mutuelle + "\\OK_MANUEL";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbUnknown++;
                }
            }

            if(nbGood+nbUnknown != 0){

                total += nbGood + nbUnknown;
                listStats.push_back (mutuelle);
                listStats.push_back (QString::number (nbGood));
                listStats.push_back (QString::number (nbUnknown));
                listStats.push_back (QString::number (nbGood+nbUnknown));
            }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);


}























//----------------------------------------------------------------------------------------------------------------------
QStringList Trieur::trierAPI (QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryList;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);


    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext () ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;
        dossier = info.absoluteFilePath () + "\\OK";
        entryList = dossier.entryInfoList (QDir::Files | QDir::NoDotAndDotDot);
        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        nbGood = entryList.length ();

        dossier = pathU + "\\" + mutuelle;
        entryList = dossier.entryInfoList (QDir::Files);
        nbUnknown = entryList.length ();


        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}














QStringList Trieur::trierAPIDate (QDate date, QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::AllEntries | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;

        dossier = info.absoluteFilePath () + "\\OK\\" % date.toString ("yyyyMMdd");
        entryListInfo = dossier.entryInfoList (QDir::Files);

        nbGood = entryListInfo.length ();

        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}




















QStringList Trieur::trierAPIPeriode (QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;

    QDir dossier;
    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

            nbGood = 0;
            nbUnknown = 0;

            info = dirOC.next () ;

            dossier = info.absoluteFilePath () + "\\OK";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            path = info.absoluteFilePath ();
            mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbGood++;
                }
            }

            if(nbGood+nbUnknown != 0){

                total += nbGood + nbUnknown;
                listStats.push_back (mutuelle);
                listStats.push_back (QString::number (nbGood));
                listStats.push_back (QString::number (nbUnknown));
                listStats.push_back (QString::number (nbGood+nbUnknown));
            }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);
}






//----------------------------------------------------------------------------------------------------------------------
QStringList Trieur::trierNR (QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryList;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);


    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext () ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;
        dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
        entryList = dossier.entryInfoList (QDir::Files | QDir::NoDotAndDotDot);
        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        nbGood = entryList.length ();

        dossier = pathU + "\\" + mutuelle + "\\NR";
        entryList = dossier.entryInfoList (QDir::Files);
        nbUnknown = entryList.length ();


        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}













QStringList Trieur::trierNRDate (QDate date, QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::AllEntries | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;

        dossier = info.absoluteFilePath () + "\\ERREUR\\NR\\" % date.toString ("yyyyMMdd");
        entryListInfo = dossier.entryInfoList (QDir::Files);

        nbGood = entryListInfo.length ();

        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());


        dossier = pathU + "\\" + mutuelle + "\\NR\\" % date.toString ("yyyyMMdd");
        entryListInfo = dossier.entryInfoList (QDir::Files);

        nbUnknown = entryListInfo.length ();

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}











QStringList Trieur::trierNRPeriode (QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total){

    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryListInfo;

    QDir dossier;
    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext() ){

            nbGood = 0;
            nbUnknown = 0;

            info = dirOC.next () ;

            dossier = info.absoluteFilePath () + "\\ERREUR\\NR";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            path = info.absoluteFilePath ();
            mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbGood++;
                }
            }

            dossier = pathU + "\\" + mutuelle + "\\NR";
            entryListInfo = dossier.entryInfoList (QDir::Files);

            if( !entryListInfo.isEmpty () ){

                for(int i = 0 ; i < entryListInfo.length () ; i++){

                    if( entryListInfo.at (i).created ().toString ("yyyyMMdd") >= date_p1.toString ("yyyyMMdd") && entryListInfo.at (i).created ().date ().toString ("yyyyMMdd") <= date_p2.toString ("yyyyMMdd"))
                        nbUnknown++;
                }
            }

            if(nbGood+nbUnknown != 0){

                total += nbGood + nbUnknown;
                listStats.push_back (mutuelle);
                listStats.push_back (QString::number (nbGood));
                listStats.push_back (QString::number (nbUnknown));
                listStats.push_back (QString::number (nbGood+nbUnknown));
            }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}





//----------------------------------------------------------------------------------------------------------------------
QStringList Trieur::trierRecv (QLineEdit *IHM_Ligne_Total){


    uint nbGood = 0;
    uint nbUnknown = 0;
    uint total = 0;

    QString mutuelle;
    QString path;

    QStringList listStats;
    QFileInfoList entryList;
    QDir dossier;

    QFileInfo info;

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);


    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while( dirOC.hasNext () ){

        nbGood = 0;
        nbUnknown = 0;

        info = dirOC.next () ;
        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList (QDir::Files | QDir::NoDotAndDotDot);
        path = info.absoluteFilePath ();
        mutuelle = path.mid (path.lastIndexOf ("/") + 1 , path.length ());

        nbGood = entryList.length ();

        if(nbGood+nbUnknown != 0){

            total += nbGood + nbUnknown;
            listStats.push_back (mutuelle);
            listStats.push_back (QString::number (nbGood));
            listStats.push_back (QString::number (nbUnknown));
            listStats.push_back (QString::number (nbGood+nbUnknown));
        }
    }

    IHM_Ligne_Total->setText (QString::number (total));
    return(listStats);

}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Sp�cifique � la r�conciliation via le SuperAdmin
  */
QList<QStringList> Trieur::trierErreur_admin (){

    uint nbGood = 0;

    QString mutuelle;

    QList<QStringList> listeStats;
    QStringList liste;

    QFileInfo info = pathG;
    QDir dossier = info.absoluteFilePath ();
    QFileInfoList entryList = dossier.entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    QDirIterator dirOC(pathG, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence
    while( dirOC.hasNext () ){

        liste.clear ();

        nbGood = 0;

        //On r�cup�re les infos du dossier
        info = dirOC.next () ;

        mutuelle = info.baseName ();

        dossier = info.absoluteFilePath ();
        entryList = dossier.entryInfoList ( QDir::Files | QDir::NoDotAndDotDot );

        //On r�cup�re le nombre de fichier dans ce dossier [GoodNIN]
        nbGood = entryList.length ();

        if(nbGood != 0){

            liste.push_back ( mutuelle );
            liste.push_back ( QString::number ( nbGood ) );

            listeStats.push_back (liste);
        }
    }
    trierListe ( listeStats );
    return( listeStats );
}



//----------------------------------------------------------------------------------------------------------------------
void Trieur::trierListe (QList<QStringList> &liste){

    for( int k = 0 ; k < liste.length () ; k++){

        for(int i = 0 ; i < liste.length () - 1 ; i++){

            if( liste[i].at (1).toInt () < liste[i+1].at (1).toInt () )
                liste.swap (i,i+1);
        }
    }
}
