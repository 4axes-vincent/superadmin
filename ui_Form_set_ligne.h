/********************************************************************************
** Form generated from reading UI file 'Form_set_ligne.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_SET_LIGNE_H
#define UI_FORM_SET_LIGNE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_set_ligne
{
public:
    QLineEdit *lineEdit_provider;
    QCheckBox *checkBox_decoupage;
    QPushButton *btn_annuler;
    QLineEdit *lineEdit_numFax;
    QLineEdit *lineEdit_rnm;
    QCheckBox *checkBox_DLL;
    QPushButton *btn_valider;
    QCheckBox *checkBox_ignorer;
    QLabel *label_provider;
    QLabel *labelnumFax;
    QLabel *label_nrm_oc;

    void setupUi(QWidget *Form_set_ligne)
    {
        if (Form_set_ligne->objectName().isEmpty())
            Form_set_ligne->setObjectName(QString::fromUtf8("Form_set_ligne"));
        Form_set_ligne->resize(1070, 153);
        lineEdit_provider = new QLineEdit(Form_set_ligne);
        lineEdit_provider->setObjectName(QString::fromUtf8("lineEdit_provider"));
        lineEdit_provider->setGeometry(QRect(700, 50, 231, 20));
        checkBox_decoupage = new QCheckBox(Form_set_ligne);
        checkBox_decoupage->setObjectName(QString::fromUtf8("checkBox_decoupage"));
        checkBox_decoupage->setGeometry(QRect(560, 50, 101, 17));
        btn_annuler = new QPushButton(Form_set_ligne);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(910, 120, 75, 23));
        lineEdit_numFax = new QLineEdit(Form_set_ligne);
        lineEdit_numFax->setObjectName(QString::fromUtf8("lineEdit_numFax"));
        lineEdit_numFax->setGeometry(QRect(120, 50, 161, 20));
        lineEdit_rnm = new QLineEdit(Form_set_ligne);
        lineEdit_rnm->setObjectName(QString::fromUtf8("lineEdit_rnm"));
        lineEdit_rnm->setGeometry(QRect(320, 50, 191, 20));
        checkBox_DLL = new QCheckBox(Form_set_ligne);
        checkBox_DLL->setObjectName(QString::fromUtf8("checkBox_DLL"));
        checkBox_DLL->setGeometry(QRect(980, 50, 70, 17));
        btn_valider = new QPushButton(Form_set_ligne);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(810, 120, 75, 23));
        checkBox_ignorer = new QCheckBox(Form_set_ligne);
        checkBox_ignorer->setObjectName(QString::fromUtf8("checkBox_ignorer"));
        checkBox_ignorer->setGeometry(QRect(40, 50, 70, 17));
        label_provider = new QLabel(Form_set_ligne);
        label_provider->setObjectName(QString::fromUtf8("label_provider"));
        label_provider->setGeometry(QRect(780, 20, 46, 13));
        labelnumFax = new QLabel(Form_set_ligne);
        labelnumFax->setObjectName(QString::fromUtf8("labelnumFax"));
        labelnumFax->setGeometry(QRect(170, 20, 81, 16));
        label_nrm_oc = new QLabel(Form_set_ligne);
        label_nrm_oc->setObjectName(QString::fromUtf8("label_nrm_oc"));
        label_nrm_oc->setGeometry(QRect(390, 20, 51, 16));

        retranslateUi(Form_set_ligne);

        QMetaObject::connectSlotsByName(Form_set_ligne);
    } // setupUi

    void retranslateUi(QWidget *Form_set_ligne)
    {
        Form_set_ligne->setWindowTitle(QApplication::translate("Form_set_ligne", "Form", 0, QApplication::UnicodeUTF8));
        checkBox_decoupage->setText(QApplication::translate("Form_set_ligne", "D\303\251coupage", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("Form_set_ligne", "Annuler", 0, QApplication::UnicodeUTF8));
        checkBox_DLL->setText(QApplication::translate("Form_set_ligne", "gs32", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Form_set_ligne", "Valider", 0, QApplication::UnicodeUTF8));
        checkBox_ignorer->setText(QApplication::translate("Form_set_ligne", "Ignorer", 0, QApplication::UnicodeUTF8));
        label_provider->setText(QApplication::translate("Form_set_ligne", "Provider", 0, QApplication::UnicodeUTF8));
        labelnumFax->setText(QApplication::translate("Form_set_ligne", "Num\303\251ro de fax", 0, QApplication::UnicodeUTF8));
        label_nrm_oc->setText(QApplication::translate("Form_set_ligne", "RNM_OC", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_set_ligne: public Ui_Form_set_ligne {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_SET_LIGNE_H
