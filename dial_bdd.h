#ifndef DIAL_BDD_H
#define DIAL_BDD_H

#include <QStringList>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlResult>
#include <QStringBuilder>
#include <QDate>
#include <QMessageBox>

class Dial_bdd
{
public:
    Dial_bdd(QString dataBaseName);

    void importerTraceFax(QList<QStringList> &listeStats, bool siJournee, QString date_1, QString date_2);
    void importerDetailReconciliation(QList<QStringList> &listeInfo, QString VM);

    QVector<double> importerStats(QString contexte, QString typeAPI, QString date);
    QVector<double> importerStats(QString contexte, QString typeAPI, QString dateDebut, QString dateFin);

    void debugger(QString titre, QString message);

private:

    QString pathBaseDonnees;
    QSqlDatabase db;
};

#endif // DIAL_BDD_H
