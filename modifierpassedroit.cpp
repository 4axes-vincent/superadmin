#include "modifierpassedroit.h"
#include "ui_modifierpassedroit.h"

modifierPasseDroit::modifierPasseDroit(QString VM, QString nomMutuelle, QString passeDroit, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::modifierPasseDroit)
{
    ui->setupUi(this);

    this->setFixedSize (479,177);
    this->setWindowTitle ("Modification passe droit - " % nomMutuelle);

    machine = VM;
    mutuelle = nomMutuelle;
    passeDroitMutuelle = passeDroit;

    ui->lineEdit_nomMutuelle->setReadOnly ( true );

    ui->lineEdit_nomMutuelle->setText ( nomMutuelle );
    ui->lineEdit_passeDroit->setText ( passeDroit );
}

modifierPasseDroit::~modifierPasseDroit()
{
    delete ui;
}



//----------------------------------------------------------------------------------------------------------------------
void modifierPasseDroit::on_btn_annuler_clicked(){

    close();
}




//----------------------------------------------------------------------------------------------------------------------
void modifierPasseDroit::on_btn_valider_clicked(){

    if( !ui->lineEdit_passeDroit->text ().isEmpty () ){

        if( ui->lineEdit_passeDroit->text ().contains ("-") ){

            QString pathPDFScrute = config->getPathPDFScrute ();

            if( machine == "4AXES_35" )
                pathPDFScrute.replace ("$","");

            QSettings settings("\\\\" % config->getAdrIP ( machine ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                               ,QSettings::IniFormat);
            settings.beginGroup ("Spec");

            settings.setValue ( mutuelle, ui->lineEdit_passeDroit->text () );

            settings.endGroup ();

            QMessageBox::information (NULL,"Passe droit","Modifcation termin�e");

            emit succesEditPasseDroit ( ui->lineEdit_passeDroit->text () );

            close();
        }
        else
            QMessageBox::warning (NULL,"Caract�re manquant", "Veuillez ne pas oublier le caract�re \" - \"");
    }
    else
        QMessageBox::warning (NULL,"Passe droit", "Veuillez indiqu� un/des passes droits");
}
