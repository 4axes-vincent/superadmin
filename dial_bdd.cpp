#include "dial_bdd.h"


#define UTILISATEUR_BDD             QString("4axes")
#define MOT_DE_PASSE_BDD            QString("fgbmo78")
#define DATABASE                    QString("QSQLITE")
#define HOSTNAME                    QString("localhost")


Dial_bdd::Dial_bdd(QString dataBaseName){

    pathBaseDonnees = dataBaseName;
}




//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer la trace de fax collecter et de mettre � jour la liste de liste contenant les informations
  */
void Dial_bdd::importerTraceFax(QList<QStringList> &listeStats, bool siJournee, QString date_1, QString date_2){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( pathBaseDonnees );

    if( ! db.open() )
        debugger ("Ouverture base de donn�es",
                  "Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text() );

    else{


        QStringList stats;

        QSqlQuery sqlQuery;
        QString requete;

        QDate dateDebut;
        QString dateD = dateDebut.fromString (date_1 , "dd/MM/yyyy").toString ("yyyyMMdd");

        //Si l'utilisateur effectue une recherche par journ�e
        if( siJournee )
            requete = "SELECT numeroFax, mutuelle, occurence FROM tbl_statsPopper WHERE date='" % dateD % "';";
        else if ( !siJournee ){

            QDate dateFin;
            QString dateF = dateFin.fromString ( date_2, "dd/MM/yyyy").toString ("yyyyMMdd");

             requete = "SELECT numeroFax, mutuelle, occurence FROM tbl_statsPopper WHERE date >= '" % dateD % "' AND "
                     " date <= '" % dateF % "';";
        }

        if( !sqlQuery.exec ( requete ) )
            debugger ("Ouverture base de donn�es",
                      "Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : " % sqlQuery.lastError ().text ());

        else{

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                stats.clear ();
                stats.push_back ( sqlQuery.value (0).toString () );
                stats.push_back ( sqlQuery.value (1).toString () );
                stats.push_back ( sqlQuery.value (2).toString () );

                listeStats.push_back ( stats );
            }
        }

        //Cloture de la connexion
        db.commit();
        db.removeDatabase ( pathBaseDonnees  );
        db.close();
    }
}









//----------------------------------------------------------------------------------------------------------------------
void Dial_bdd::importerDetailReconciliation(QList<QStringList> &listeInfos, QString VM){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( pathBaseDonnees );

    if( ! db.open() )
        debugger ("Ouverture base de donn�es",
                  pathBaseDonnees % " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text() );

    else{

        QStringList listeInfo;
        QSqlQuery sqlQuery;
        QString requete;

        requete = "SELECT nomFichier,"
                " dateReconciliation,"
                " heureReconciliation,"
                " nomOperatrice,"
                " vm,"
                " action,"
                " nomPatient,"
                " prenomPatient,"
                " numDossier,"
                " dateEntree,"
                " CH,"
                " mutuelleDpec,"
                " mutuelleVm FROM tbl_messageReconcilie";

        if( !sqlQuery.exec ( requete ) )
            debugger ("Ex�cution requ�te",VM % " -> Erreur ex�cution requ�te [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
        else{

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                listeInfo.clear ();
                listeInfo.push_back ( sqlQuery.value (0).toString () );
                listeInfo.push_back ( sqlQuery.value (1).toString () );
                listeInfo.push_back ( sqlQuery.value (2).toString () );
                listeInfo.push_back ( sqlQuery.value (3).toString () );
                listeInfo.push_back ( sqlQuery.value (4).toString () );
                listeInfo.push_back ( sqlQuery.value (5).toString () );
                listeInfo.push_back ( sqlQuery.value (6).toString () );
                listeInfo.push_back ( sqlQuery.value (7).toString () );
                listeInfo.push_back ( sqlQuery.value (8).toString () );
                listeInfo.push_back ( sqlQuery.value (9).toString () );
                listeInfo.push_back ( sqlQuery.value (10).toString () );
                listeInfo.push_back ( sqlQuery.value (11).toString () );
                listeInfo.push_back ( sqlQuery.value (12).toString () );

                listeInfos.push_back ( listeInfo );
            }
        }

        //Cloture de la connexion
        db.commit();
        db.removeDatabase ( pathBaseDonnees  );
        db.close();
    }
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer les donn�es depuis la base de donn�es des statistiques
  */
QVector<double> Dial_bdd::importerStats(QString contexte, QString typeAPI, QString date){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( pathBaseDonnees );

    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;

        QVector<double> vecteurDonnee;

        requete = "SELECT temps FROM tbl_stats WHERE typeAPI='" % typeAPI % "' AND date='" % date % "' AND contexte='" % contexte % "';";

        if( !sqlQuery.exec ( requete ) )
            debugger ("Erreur requ�te", "La requ�te [ " % requete % " ] n'a pas pu s'ex�cuter | Erreur : " % sqlQuery.lastError ().text () );

        else{

            //On r�uni les valeurs
            while( sqlQuery.next () ){

                vecteurDonnee.push_back (sqlQuery.value (0).toString ().toFloat ()/ 1000);
            }
            sqlQuery.clear ();

            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( pathBaseDonnees  );
            db.close();

            return( vecteurDonnee );
        }
    }
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer les donn�es depuis la base de donn�es des statistiques
  */
QVector<double> Dial_bdd::importerStats(QString contexte, QString typeAPI, QString dateDebut, QString dateFin){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( pathBaseDonnees );

    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;

        QVector<double> vecteurDonnee;

        requete = "SELECT temps FROM tbl_stats WHERE typeAPI='" % typeAPI % "' AND contexte='" % contexte % "'"
                " AND date>='" % dateDebut % "' AND date<='" % dateFin % "';";

        if( !sqlQuery.exec ( requete ) )
            debugger ("Erreur requ�te", "La requ�te [ " % requete % " ] n'a pas pu s'ex�cuter | Erreur : " % sqlQuery.lastError ().text () );

        else{

            //On r�uni les valeurs
            while( sqlQuery.next () ){

                vecteurDonnee.push_back (sqlQuery.value (0).toString ().toFloat () / 1000);
            }
            sqlQuery.clear ();


            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( pathBaseDonnees  );
            db.close();

            return( vecteurDonnee );
        }
    }
}

















//----------------------------------------------------------------------------------------------------------------------
void Dial_bdd::debugger(QString titre, QString message){

    QMessageBox::information (NULL, titre, message);
}
