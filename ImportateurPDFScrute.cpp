#include "ImportateurPDFScrute.h"

ImportateurPDFScrute::ImportateurPDFScrute(){

    ok = true;

}







//-----------------------------------------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer le PDFScrute.ini d'une VM
  Un backup est fait � chaque importation (fichier IN)
  Pour des raisons de manipulation ("\\")une copie de se fichier est faite (fichier OUT)

  Chaque information (Nom mutuelle, login, mdp, repOk, repErreur, RepRecup) sont stock�e
  dans une QStringList, chaque QStringList sera ins�r�e dans une liste de QStringList
  */
QList<QStringList> ImportateurPDFScrute::importerDonnee(QString VM){


    QList<QStringList>         listData;

    if( ! VM.isEmpty() ){

        Configuration *config = new Configuration();

        QString                     ligneCourante;
        QString                     adrIP           = config->getAdrIP(VM);
        QString                     path            = "\\\\" + adrIP + "\\" + config->getPathPDFScrute() + "\\PDFScrute.ini";
//        QString                     path            = config->getPathPDFScrute() + "\\" + VM + "\\PDFScrute.ini" ;
        QString                     pathOUT         = config->getPathBackup() + "\\" + "PDFScrute_"+ VM + ".OUT";

        //On backup le fichier d'origine au moment de l'importation
        sauvegarderFichier_IN(VM, adrIP, config);
        sauvegarderFichier_OUT(VM, adrIP,config);

        //Ouverture fichier
        QFile fichierEntree(path);
        if ( ! fichierEntree.open(QIODevice::ReadWrite | QIODevice::Text) )
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","ERREUR - Probleme ouverture fichier PDFScrute.ini OUT de la "+VM);

        else{

            //Ouverture flux
            QTextStream fluxEntrant(&fichierEntree);

            while ( ! fluxEntrant.atEnd() ){

                //Lecture ligne courante
                ligneCourante = fluxEntrant.readLine();

                //On charge les donn�es dans la structure
                chargerDonnee(ligneCourante, pathOUT, listData, config, fluxEntrant);
            }

            //Fermeture fichier
            fichierEntree.close();
        }
    }
    return(listData);
}























//-----------------------------------------------------------------------------------------------------------------------------------------------------
/*
  Permet de sauvegarder le fichier import� dans le dossier Backup
  Identification du fichier : VM, IN, date+heure
  */
void ImportateurPDFScrute::sauvegarderFichier_IN(QString VM, QString adrIP,Configuration *config){

    QDateTime           dateTime;

//    QFile::copy(config->getPathPDFScrute() + "\\" + VM + "\\PDFScrute.ini", config->getPathBackup() + "\\" + "PDFScrute_"+ VM + "_IN_" + dateTime.currentDateTime().toString("yyyyMMdd_hh_mm_ss").append(".ini"));
    QFile::copy("\\\\" + adrIP + "\\" + config->getPathPDFScrute() + "\\PDFScrute.ini", config->getPathBackup() + "\\" + "PDFScrute_"+ VM + "_IN_" + dateTime.currentDateTime().toString("yyyyMMdd_hh_mm_ss").append(".ini"));

}













//-----------------------------------------------------------------------------------------------------------------------------------------------------
/*
  Permet de sauvegarder le fichier import� dans le dossier Backup pour son utilisation directe
  Identification du fichier : VM, OUT
  */
void ImportateurPDFScrute::sauvegarderFichier_OUT(QString VM, QString adrIP, Configuration *config){

//    QFile::copy(config->getPathPDFScrute() + "\\" + VM + "\\PDFScrute.ini", config->getPathBackup() + "\\" + "PDFScrute_"+ VM + ".OUT");
    QFile::copy("\\\\" + adrIP + "\\" + config->getPathPDFScrute() + "\\PDFScrute.ini", config->getPathBackup() + "\\" + "PDFScrute_"+ VM + ".OUT");


}














//-----------------------------------------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ins�rer l'information dans une structure
  */
void ImportateurPDFScrute::chargerDonnee(QString ligneCourante, QString pathOUT, QList< QStringList > &listData, Configuration *config, QTextStream &flux){

    if( ligneCourante.contains("[") || ligneCourante.contains("]") ){

        QStringList liste_detail;

        if( !ligneCourante.contains("CONFIG") ){

                liste_detail.push_back(ligneCourante);

                ligneCourante = flux.readLine();
                liste_detail.push_back(ligneCourante.mid(ligneCourante.indexOf("=")+1,ligneCourante.length()));

                ligneCourante = flux.readLine();
                liste_detail.push_back(ligneCourante.mid(ligneCourante.indexOf("=")+1,ligneCourante.length()));

                ligneCourante = flux.readLine();
                liste_detail.push_back(ligneCourante.mid(ligneCourante.indexOf("=")+1,ligneCourante.length()));

                ligneCourante = flux.readLine();
                liste_detail.push_back(ligneCourante.mid(ligneCourante.indexOf("=")+1,ligneCourante.length()));

                ligneCourante = flux.readLine();
                liste_detail.push_back(ligneCourante.mid(ligneCourante.indexOf("=")+1,ligneCourante.length()));

                //On ajoute la QStringList � la QList
                listData.push_back( liste_detail );
        }
    }
}






//-----------------------------------------------------------------------------------------------------------------------------------------------------
void ImportateurPDFScrute::remplacerSlash(QString path_IN, QString path_OUT, QString VM){

    QString texte;

    //Ouverture du fichier � analyser
    QFile fichierOrigine(path_IN);

    if ( ! fichierOrigine.open(QIODevice::ReadWrite | QIODevice::Text) )
        QMessageBox::warning(NULL,"Probl�me ouverture fichier","ERREUR - Probleme ouverture fichier PDFScrute.ini IN de la "+VM);

    else{

        //Ouverture flux
        QTextStream fluxOrigine(&fichierOrigine);

        //Modification de la chaine - QSettings interpr�te le "\" comme un caract�re sp�cial
        texte = fluxOrigine.readAll();
        texte.replace("\\","\\\\");

        fichierOrigine.close();

        //Ouverture fichier
        QFile fichierOUT(path_OUT);

        if ( ! fichierOUT.open(QIODevice::ReadWrite | QIODevice::Text) )
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","ERREUR - Probleme ouverture fichier PDFScrute.ini OUT de la "+VM);

        else{

            //Ouverture flux
            QTextStream fluxOUT(&fichierOUT);

            fluxOUT << texte ;
            fichierOUT.close();
        }
    }
}
