#include "Form_ligne_tab_gauche.h"
#include "ui_Form_ligne_tab_gauche.h"

Form_ligne_tab_gauche::Form_ligne_tab_gauche(QString new_VM, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_ligne_tab_gauche)
{
    ui->setupUi(this);
    VM = new_VM;
    initialiser();

}

Form_ligne_tab_gauche::~Form_ligne_tab_gauche(){

    delete ui;
}

void Form_ligne_tab_gauche::initialiser(){

    this->setWindowTitle("Nouvelle ligne");
    this->setFixedSize(1070,153);

}





void Form_ligne_tab_gauche::ajouterLigne(QString ignorer, QString numFax, QString rnm, QString decoupage, QString provider, QString gs32){

    Configuration *config = new Configuration();

    QFile fichierSortant(config->getPathMapping() + "\\" + VM + "\\mapping.csv");
    if( !fichierSortant.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append) ){

        QMessageBox::warning(NULL,"Probl�me ouverture fichier","Impossible d'ouvrir le mapping de la " +VM);
    }
    else{

        QTextStream fluxSortant(&fichierSortant);

        fluxSortant << ignorer << numFax << ";" << rnm << ";" << decoupage << ";" << provider << ";" << gs32 << endl;
    }

    fichierSortant.close();
}




void Form_ligne_tab_gauche::on_btn_annuler_clicked(){

    close();
}

void Form_ligne_tab_gauche::on_btn_valider_clicked(){



    QString ignorer             = "";
    QString decoupage           = "";
    QString gs32                = "";

    QString numFax              = ui->lineEdit_numFax->text();
    QString rnm                 = ui->lineEdit_rnm->text();
    QString provider            = ui->lineEdit_provider->text();

    if( !numFax.isEmpty() && !rnm.isEmpty() && !provider.isEmpty() ){

        if( ui->checkBox_ignorer->isChecked() ){

            ignorer = ";";
        }
        if( ui->checkBox_decoupage->isChecked() ){

            decoupage = "*";
        }
        if( ui->checkBox_DLL->isChecked() ){

            gs32 = "gs32";
        }

        ajouterLigne(ignorer, numFax, rnm, decoupage, provider, gs32);
    }
    else{

        QMessageBox::warning(NULL,"Information manquante","Veuillez remplir tous les champs !");

    }

}
