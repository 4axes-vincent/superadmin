#ifndef AJOUTER_VM_H
#define AJOUTER_VM_H

#include <QWidget>
#include <QMessageBox>
#include <QSettings>

namespace Ui {
class Ajouter_VM;
}

class Ajouter_VM : public QWidget
{
    Q_OBJECT
    
public:
    explicit Ajouter_VM(QWidget *parent = 0);
    ~Ajouter_VM();

    void initialiser();
    void add_VM();

private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();

private:
    Ui::Ajouter_VM *ui;
};

#endif // AJOUTER_VM_H
