/********************************************************************************
** Form generated from reading UI file 'ajouterpdfscrute.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AJOUTERPDFSCRUTE_H
#define UI_AJOUTERPDFSCRUTE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ajouterPDFScrute
{
public:
    QLineEdit *lineEdit_mutuelle;
    QRadioButton *radioButton_active;
    QRadioButton *radioButton_inactive;
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *ajouterPDFScrute)
    {
        if (ajouterPDFScrute->objectName().isEmpty())
            ajouterPDFScrute->setObjectName(QString::fromUtf8("ajouterPDFScrute"));
        ajouterPDFScrute->resize(222, 201);
        lineEdit_mutuelle = new QLineEdit(ajouterPDFScrute);
        lineEdit_mutuelle->setObjectName(QString::fromUtf8("lineEdit_mutuelle"));
        lineEdit_mutuelle->setGeometry(QRect(20, 80, 181, 20));
        radioButton_active = new QRadioButton(ajouterPDFScrute);
        radioButton_active->setObjectName(QString::fromUtf8("radioButton_active"));
        radioButton_active->setGeometry(QRect(30, 120, 82, 17));
        radioButton_inactive = new QRadioButton(ajouterPDFScrute);
        radioButton_inactive->setObjectName(QString::fromUtf8("radioButton_inactive"));
        radioButton_inactive->setGeometry(QRect(130, 120, 82, 17));
        btn_valider = new QPushButton(ajouterPDFScrute);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(20, 150, 75, 23));
        btn_annuler = new QPushButton(ajouterPDFScrute);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(130, 150, 75, 23));
        label = new QLabel(ajouterPDFScrute);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 10, 141, 16));
        label_2 = new QLabel(ajouterPDFScrute);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(80, 50, 61, 16));

        retranslateUi(ajouterPDFScrute);

        QMetaObject::connectSlotsByName(ajouterPDFScrute);
    } // setupUi

    void retranslateUi(QWidget *ajouterPDFScrute)
    {
        ajouterPDFScrute->setWindowTitle(QApplication::translate("ajouterPDFScrute", "Form", 0, QApplication::UnicodeUTF8));
        radioButton_active->setText(QApplication::translate("ajouterPDFScrute", "Active", 0, QApplication::UnicodeUTF8));
        radioButton_inactive->setText(QApplication::translate("ajouterPDFScrute", "Inactive", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("ajouterPDFScrute", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("ajouterPDFScrute", "Annuler", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ajouterPDFScrute", "Ajouter donn\303\251es PDFScrute", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ajouterPDFScrute", "Mutuelle :", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ajouterPDFScrute: public Ui_ajouterPDFScrute {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AJOUTERPDFSCRUTE_H
