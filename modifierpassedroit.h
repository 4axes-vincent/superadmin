#ifndef MODIFIERPASSEDROIT_H
#define MODIFIERPASSEDROIT_H

#include <QWidget>
#include <QSettings>
#include <QStringBuilder>
#include <QMessageBox>

#include "Configuration.h"

namespace Ui {
class modifierPasseDroit;
}

class modifierPasseDroit : public QWidget
{
    Q_OBJECT
    
public:
    explicit modifierPasseDroit(QString VM, QString nomMutuelle, QString passeDroit, QWidget *parent = 0);
    ~modifierPasseDroit();
    
private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();

signals:

    void succesEditPasseDroit(QString passeDroit);

private:
    Ui::modifierPasseDroit *ui;

    Configuration *config;

    QString machine;
    QString mutuelle;
    QString passeDroitMutuelle;
};

#endif // MODIFIERPASSEDROIT_H
