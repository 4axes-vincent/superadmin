#ifndef AJOUTERPDFSCRUTE_H
#define AJOUTERPDFSCRUTE_H

#include "Configuration.h"

#include <QWidget>
#include <QSettings>
#include <QStringBuilder>
#include <QMessageBox>

namespace Ui {
class ajouterPDFScrute;
}

class ajouterPDFScrute : public QWidget
{
    Q_OBJECT
    
public:
    explicit ajouterPDFScrute(QString VM, QWidget *parent = 0);
    ~ajouterPDFScrute();

    void debugger(QString titre, QString information);

    void ajouterDonnee(QString nomMutuelle, QString activite, QString VM);
        
private slots:
    void on_btn_valider_clicked();

    void on_btn_annuler_clicked();

signals:
    void succesInsert(QString nomMutuelle, QString activiteMutuelle);

private:
    Ui::ajouterPDFScrute *ui;
    Configuration *config;

    QString machine;
};

#endif // AJOUTERPDFSCRUTE_H
