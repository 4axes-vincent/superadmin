/********************************************************************************
** Form generated from reading UI file 'Ajouter_VM.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AJOUTER_VM_H
#define UI_AJOUTER_VM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Ajouter_VM
{
public:
    QLineEdit *lineEdit_numVM;
    QLineEdit *lineEdit_IP;
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *label_titre;
    QLabel *label_numVM;
    QLabel *label_IP;
    QLabel *label_exemple1;

    void setupUi(QWidget *Ajouter_VM)
    {
        if (Ajouter_VM->objectName().isEmpty())
            Ajouter_VM->setObjectName(QString::fromUtf8("Ajouter_VM"));
        Ajouter_VM->resize(317, 210);
        lineEdit_numVM = new QLineEdit(Ajouter_VM);
        lineEdit_numVM->setObjectName(QString::fromUtf8("lineEdit_numVM"));
        lineEdit_numVM->setGeometry(QRect(90, 60, 113, 20));
        lineEdit_IP = new QLineEdit(Ajouter_VM);
        lineEdit_IP->setObjectName(QString::fromUtf8("lineEdit_IP"));
        lineEdit_IP->setGeometry(QRect(90, 110, 113, 20));
        btn_valider = new QPushButton(Ajouter_VM);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(90, 160, 75, 23));
        btn_annuler = new QPushButton(Ajouter_VM);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(190, 160, 75, 23));
        label_titre = new QLabel(Ajouter_VM);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(120, 20, 61, 16));
        label_numVM = new QLabel(Ajouter_VM);
        label_numVM->setObjectName(QString::fromUtf8("label_numVM"));
        label_numVM->setGeometry(QRect(60, 60, 31, 16));
        label_IP = new QLabel(Ajouter_VM);
        label_IP->setObjectName(QString::fromUtf8("label_IP"));
        label_IP->setGeometry(QRect(60, 110, 31, 16));
        label_exemple1 = new QLabel(Ajouter_VM);
        label_exemple1->setObjectName(QString::fromUtf8("label_exemple1"));
        label_exemple1->setGeometry(QRect(220, 60, 81, 16));

        retranslateUi(Ajouter_VM);

        QMetaObject::connectSlotsByName(Ajouter_VM);
    } // setupUi

    void retranslateUi(QWidget *Ajouter_VM)
    {
        Ajouter_VM->setWindowTitle(QApplication::translate("Ajouter_VM", "Form", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Ajouter_VM", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("Ajouter_VM", "Annuler", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Ajouter_VM", "Ajouter V.M", 0, QApplication::UnicodeUTF8));
        label_numVM->setText(QApplication::translate("Ajouter_VM", "VM :", 0, QApplication::UnicodeUTF8));
        label_IP->setText(QApplication::translate("Ajouter_VM", "IP :", 0, QApplication::UnicodeUTF8));
        label_exemple1->setText(QApplication::translate("Ajouter_VM", "( Exemple : 35 )", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Ajouter_VM: public Ui_Ajouter_VM {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AJOUTER_VM_H
