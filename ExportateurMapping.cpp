#include "exportateurMapping.h"

exportateurMapping::exportateurMapping(){

}





//----------------------------------------------------------------------------------------------------------------------
/*
  Met � jour le fichier temporaire OUT d'une VM concern�e
  Param�tre d'entr�e :

  QString VM => VM concern�e
  QStringList listInfo => liste contenant les lignes � ins�rer
  */
void exportateurMapping::exporterToOut(QString VM, QStringList listInfo){

    Configuration *config = new Configuration();

    QString                 path            = config->getPathBackup() + "\\mapping_" + VM + ".OUT";
    QString                 data;

    QFile fichierSortie(path);
    if( ! fichierSortie.open(QIODevice::Text | QIODevice::WriteOnly) )
         QMessageBox::warning(NULL,"Ouverture fichier","Impossible d'ouvrir le mapping OUT de la " + VM);

    else{

        QTextStream fluxSortant(&fichierSortie);

        for( int i = 0 ; i < listInfo.length() ; i++){

            listInfo[i].replace("Non;","");             //Si la ligne commence par "Non" -> non ignorer, on supprime le Non + le ";"
            listInfo[i].replace("Oui","");              //Si la ligne commence par Oui -> ignorer, on supprime le Oui et on laisse le ";"

            //Concat�nation des informations
            data += listInfo.at(i) + "\n";
        }

        //Enregistrement des modifications dans le fichier OUT
        fluxSortant << data;

        //Fermeture du fichier
        fichierSortie.close();
    }
}




























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de mettre � jour le fichier temporaire OUT de la VM concern�e
  Param�tre d'entr�e :

  QString VM => la VM concern�e
  QString donnees => Les donn�es � ins�rer en cas d'ajout
  QString new_ligne => la nouvelle ligne en cas de modification
  QString old_ligne => l'ancienne ligne en cas de modification
  contexte => deux choix possible : AJOUTER / MODIFIER
  */
void exportateurMapping::exporterToOut(QString VM, QString donnees, QString new_ligne, QString old_ligne, QString contexte){

    Configuration       *config         = new Configuration();
    QString             path            = config->getPathBackup() +  "\\mapping_" + VM + ".OUT";
    QString             texte;

    if( contexte == "ajouter" ){

        QFile fichierSortant(path);
        if( !fichierSortant.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append) )
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","Impossible d'ouvrir le mapping de la " +VM);

        else{

            QTextStream fluxSortant(&fichierSortant);
            fluxSortant << endl;
            fluxSortant << donnees;
        }
        fichierSortant.close();

    }
    else if( contexte == "modifier" ){

        //PREMIERE PARTIE
        QFile fichierEntrant(path);
        if( ! fichierEntrant.open(QIODevice::Text | QIODevice::ReadOnly) )
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","Impossible d'ouvrir le fichier mapping de la "+VM);

        else{

            QTextStream fluxEntrant(&fichierEntrant);
            texte = fluxEntrant.readAll();
            fichierEntrant.close();
        }


        //Modification de la ligne
        texte.replace(old_ligne, new_ligne);


        //SECONDE PARTIE
        QFile fichierSortant(path);
        if( ! fichierSortant.open(QIODevice::Text | QIODevice::WriteOnly) )
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","Impossible d'ouvrir le fichier mapping de la "+VM);

        else{

            QTextStream fluxSortant(&fichierSortant);
            fluxSortant << texte ;
            fichierSortant.close();
        }
    }
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de mettre � jour le fichier d'origine � partir du fichier temporaire OUT
  Param�tre d'entr�e :

  QString VM => VM concern�e
  */
void exportateurMapping::exporterOutToIN(QString VM){


    if( !VM.isEmpty() ){

        Configuration           *config         = new Configuration();
        QString                 pathOUT         = config->getPathBackup() + "\\mapping_" + VM + ".OUT";
        QString                 adrIP           = config->getAdrIP(VM);
//        QString                 path            = config->getPathMapping () + "\\" + VM + "\\mapping.csv";
        QString                 path            = "\\\\" + adrIP + "\\" + config->getPathMapping() + "\\mapping.csv" ;


        if( QFile::exists(path) ){

            QString                 data;

            QFile fichierSortie_1(pathOUT);
            if( ! fichierSortie_1.open(QIODevice::Text | QIODevice::ReadOnly) )
                QMessageBox::warning(NULL,"Ouverture fichier","Impossible d'ouvrir le mapping OUT de la " + VM);

            else{

                QTextStream fluxSortant_1(&fichierSortie_1);

                //M�morisation des donn�es du fichier OUT
                data = fluxSortant_1.readAll();

                //Fermeture fichier OUT
                fichierSortie_1.close();
            }


            //Nouveau path pour la mise � jour
//            path = config->getPathMapping() + "\\" + VM + "\\mapping.csv";

            QFile fichierSortie_2(path);
            if( ! fichierSortie_2.open(QIODevice::Text | QIODevice::WriteOnly) )
                QMessageBox::warning(NULL,"Ouverture fichier","Impossible d'ouvrir le mapping de la " + VM);
            else{

                QTextStream fluxSortant_2(&fichierSortie_2);

                //Enregistrement des donn�es dans le fichier d'origine
                fluxSortant_2 << data;

                //Fermeture fichier origine
                fichierSortie_2.close();
            }

            //Suppression du fichier temporaire OUT
            QFile::remove(config->getPathBackup() + "\\mapping_" + VM + ".OUT");

            QMessageBox::information(NULL,"Confirmation d'exportation","Exportation effectu�e !");

        }
        else
            QMessageBox::warning(NULL,"Erreur exportation","Veuillez d'abord importer pour exporter !");

    }
}




