#include "mainwindow.h"
#include "ui_mainwindow.h"


#define NOM_BDD_STATS_POPPER        QString("statsPopper")
#define NOM_BDD_RECONCILIATION      QString("Base_stats")
#define PATH_FICHIER_CONFIG         QString(QCoreApplication::applicationDirPath () % "/config.ini")


//CONSTRUCTEUR
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this);
    initialiser();
}















//-----------------------------------------------------------------------------------------------------------------------------------------------------
//DESTRUCTEUR
MainWindow::~MainWindow(){

    supprimerToutFichierOUT();
    delete ui;
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de supprimer tous les fichier OUT pr�sent dans le dossier Backup
  */
void MainWindow::supprimerToutFichierOUT (){

    QDirIterator it(config->getPathBackup (), QDir::Files);
    QString file;

    while(it.hasNext()){

        //on va au prochain fichier
        file = it.next();

        //Si c'est un fichier OUT
        if( file.contains ("OUT") )
            QFile::remove (file);
    }
}
























//----------------------------------------------------------------------------------------------------------------------
//Initialise l'objet
void MainWindow::initialiser(){

    this->setWindowTitle("SuperAdmin");
    this->setFixedSize(1327,746);

    ui->btn_modifierPasseDroit->setVisible ( false );

    ui->btn_creerTEMP->setVisible (false);
    ui->lineEdit_newTEMP->setVisible (false);
    ui->label__ex_tmp->setVisible (false);

    //Nom des colonnes -> MAPPING
    header << "Ignorer"
           << "Num�ro de fax"
           << "Mutuelle"
           << "D�coupage"
           << "Provider"
           << "DLL" ;

    header_pdfScrute << "Mutuelle"
                     << "Activit�";

    //Nom de colonnes -> Recherche
    header_table_recherche << "RNM"
                           << "Fax"
                           << "VM" ;

    //Nom des colonnes -> Statistiques par VM
    header_table_oc_gd_uk << "Mutuelle"
                          << "GoodNIN"
                          << "UnknownNIN"
                          << "Total";

    header_table_oc << "Mutuelle";

    header_oc_passe_droit << "Mutuelle"
                          << "RNM autoris�";

    headerStatsPopper << "Num�ro de fax"
                      << "RNM + Nom mutuelle"
                      << "Occurence";

    headerRechercherRecon << "Fichier"
                          << "Date"
                          << "Heure"
                          << "Op�ratrice"
                          << "VM"
                          << "Action"
                          << "Nom patient"
                          << "Pr�nom patient"
                          << "Num�ro dossier"
                          << "Date entr�e"
                          << "C.H"
                          << "Mutuelle DPEC"
                          << "Mutuelle RPEC";


    //Initalisation des onglets
    ui->tabWidget->setTabText(0,"Mapping");
    ui->tabWidget->setTabText(1,"PDFScrute");
    ui->tabWidget->setTabText(2,"Rechercher");
    ui->tabWidget->setTabText(3,"Volume");
    ui->tabWidget->setTabText(4,"Cr�ation de TEMP - Passe droit");
    ui->tabWidget->setTabText(5,"Statistques r�conciliation");
    ui->tabWidget->setTabText(6,"Statistique Popper");
    ui->tabWidget->setTabText(7,"Rechercher r�conciliation");


    clickID = 0;

    listerVM();
}



















//----------------------------------------------------------------------------------------------------------------------
/*
Permet de lister les num�ros de VM � afficher dans les comboBox
*/
void MainWindow::listerVM(){

    QStringList listeVM = config->getListeCle ( PATH_FICHIER_CONFIG, "VM" );

    ui->comboBox_VM->addItems( listeVM );
    ui->comboBox_2_VM->addItems( listeVM );

    ui->comboBox_VM_pdfScrute_1->addItems( listeVM );
    ui->comboBox_VM_pdfScrute_2->addItems( listeVM );

    ui->comboBox_VM_Volume->addItems( listeVM );

    ui->comboBox_VM_TEMP->addItems( listeVM );

    ui->comboBox_passeDroit->addItems ( listeVM );
    ui->comboBox_VM_rchStatsPopper->addItems ( listeVM  );

    ui->comboBox_VM_detailReconciliation->addItems ( listeVM );
    ui->comboBox_VM_stats->addItems ( listeVM );
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Initalise le tableau [table] avec un nombre de colonne [nbColumn] et un nombre de ligne [nbRow]
  en ins�rer le header [header]
  */
void MainWindow::initialiserTableau (QTableWidget *table, int nbColumn, int nbRow, QStringList header){

    table->clear ();

    table->setColumnCount (nbColumn);
    table->setRowCount(nbRow);
    table->setHorizontalHeaderLabels (header);
    table->horizontalHeader()->setClickable(false);
    table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}

















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_actualiser_clicked(){

    listeInfoMapping_1 = new_importateurMapping->importerDonnee(VM_1);
    remplirTableWidget_1(*ui->tableWidget_1);

    listeInfoMapping_2 = new_importateurMapping->importerDonnee(VM_2);
    remplirTableWidget_2(*ui->tableWidget_2);
}


























//----------------------------------------------------------------------------------------------------------------------
/*
    Permet de remplir la QTableWidget avec les informations pass�es en param�tres
    */
void MainWindow::remplirTableWidget_1(QTableWidget &table){


    table.clear();

    //Mise � jour de la liste pour les ";" en d�but de ligne --> IGNORER
    decouperInformation_1();

    table.setRowCount( listeInfo_decoupee_1.length() / 6 );
    table.setColumnCount(6);
    table.setHorizontalHeaderLabels(header);
    table.horizontalHeader()->setClickable(false);
    table.horizontalHeader()->setResizeMode(QHeaderView::Stretch);


    for(int j = 0 ; j < listeInfo_decoupee_1.length() ; j++){

        //Cr�ation d'une celulle contenant l'information de la table de concordance
        QTableWidgetItem *cellC = new QTableWidgetItem(listeInfo_decoupee_1.at(j));

        table.setItem(0,j,cellC);

        //Ajustement cellule
        cellC->setTextAlignment(Qt::AlignCenter);
        //cellC->setFlags(Qt::ItemIsTristate);
        cellC->setTextColor("black");
    }
}























//----------------------------------------------------------------------------------------------------------------------
/*
    Permet de remplir la QTableWidget avec les informations pass�es en param�tres
    */
void MainWindow::remplirTableWidget_2(QTableWidget &table){


    table.clear();

    //Mise � jour de la liste pour les ";" en d�but de ligne --> IGNORER
    decouperInformation_2();

    table.setRowCount( listeInfo_decoupee_2.length() / 6 );
    table.setColumnCount(6);
    table.setHorizontalHeaderLabels(header);
    table.horizontalHeader()->setClickable(false);
    table.horizontalHeader()->setResizeMode(QHeaderView::Stretch);


    for(int j = 0 ; j < listeInfo_decoupee_2.length() ; j++){

        //Cr�ation d'une celulle contenant l'information de la table de concordance
        QTableWidgetItem *cellC = new QTableWidgetItem( listeInfo_decoupee_2.at(j) );

        table.setItem(0,j,cellC);

        //Ajustement cellule
        cellC->setTextAlignment(Qt::AlignCenter);
        //cellC->setFlags(Qt::ItemIsTristate);
        cellC->setTextColor("black");
    }
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur "Importer"
  N�cessite : Choisir une VM
  */
void MainWindow::on_btn_importer_1_clicked(){

    VM_1 = ui->comboBox_VM->currentText();
    listeInfoMapping_1 = new_importateurMapping->importerDonnee(VM_1);
    remplirTableWidget_1(*ui->tableWidget_1);
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_1_cellClicked(int row, int column){

    rowSelected = row;
    ligne = listeInfoMapping_1.at(row);
}

































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_add_from_clicked(){


    if( VM_2.isEmpty() && ligne.isEmpty() )
        QMessageBox::warning(NULL,"Impossible de basculer l'information","Veuillez importer le mapping cible !");
    else{

        if (QMessageBox::Yes == QMessageBox::question(NULL, "Confirmation d�placement", "�tes-vous s�r de vouloir basculer ces informations vers le Mapping de la "+ui->comboBox_2_VM->currentText()+"?", QMessageBox::Yes|QMessageBox::No)){

            QStringList ligneSplit = ligne.split(";");

            ui->tableWidget_2->setRowCount(ui->tableWidget_2->rowCount() + 1);

            for( int i = 0 ; i < ligneSplit.length() ; i++ ){

                //Cr�ation d'une celulle contenant l'information de la table de concordance
                QTableWidgetItem *cellC = new QTableWidgetItem( ligneSplit.at(i) );

                ui->tableWidget_2->setItem( ui->tableWidget_2->rowCount() - 1 , i , cellC);

                //Ajustement cellule
                cellC->setTextAlignment(Qt::AlignCenter);
                cellC->setTextColor("black");
            }
        }

        listeInfoMapping_2.push_back(ligne);
        ui->tableWidget_1->removeRow(listeInfoMapping_1.indexOf(ligne));
        listeInfoMapping_1.removeAt(listeInfoMapping_1.indexOf(ligne));

        new_exportateurMapping->exporterToOut(VM_1, listeInfoMapping_1);
        new_exportateurMapping->exporterToOut(VM_2, listeInfoMapping_2);
    }
}

































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_add_to_clicked(){

    if( VM_1.isEmpty() ){

        QMessageBox::warning(NULL,"Impossible de basculer l'information","Veuillez importer le mapping cible !");
    }
    else{

        if (QMessageBox::Yes == QMessageBox::question(NULL, "Confirmation d�placement", "�tes-vous s�r de vouloir basculer ces informations vers le Mapping de la "+ui->comboBox_2_VM->currentText()+"?", QMessageBox::Yes|QMessageBox::No)){

            QStringList ligneSplit = ligne.split(";");

            ui->tableWidget_1->setRowCount(ui->tableWidget_1->rowCount() + 1);

            for( int i = 0 ; i < ligneSplit.length() ; i++ ){

                //Cr�ation d'une celulle contenant l'information de la table de concordance
                QTableWidgetItem *cellC = new QTableWidgetItem( ligneSplit.at(i) );

                ui->tableWidget_1->setItem( ui->tableWidget_1->rowCount() - 1 , i , cellC);

                //Ajustement cellule
                cellC->setTextAlignment(Qt::AlignCenter);
                cellC->setTextColor("black");
            }
        }

        listeInfoMapping_1.push_back(ligne);
        ui->tableWidget_2->removeRow(listeInfoMapping_2.indexOf(ligne));
        listeInfoMapping_2.removeAt(listeInfoMapping_2.indexOf(ligne));

        new_exportateurMapping->exporterToOut(VM_1, listeInfoMapping_1);
        new_exportateurMapping->exporterToOut(VM_2, listeInfoMapping_2);
    }
}






























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_exporter_clicked(){

    if( !VM_1.isEmpty() || !VM_2.isEmpty() ){

        new_exportateurMapping->exporterOutToIN(VM_1);
        new_exportateurMapping->exporterOutToIN(VM_2);
    }
    else
        QMessageBox::warning(NULL,"Probl�me exportation","Veuillez importer vos mappings avant de pouvoir exporter !");


}














// _____  _____  ______ _____                 _         //
// |  __ \|  __ \|  ____/ ____|               | |       //
// | |__) | |  | | |__ | (___   ___ _ __ _   _| |_ ___  //
// |  ___/| |  | |  __| \___ \ / __| '__| | | | __/ _ \ //
// | |    | |__| | |    ____) | (__| |  | |_| | ||  __/ //
// |_|    |_____/|_|   |_____/ \___|_|   \__,_|\__\___| //






//----------------------------------------------------------------------------------------------------------------------
/*
  Permet � l'utilisateur d'importer le PDFScrute
  */
void MainWindow::on_btn_importer_pdfscrute1_clicked(){

    //Initalisation tableau
    ui->tableWidget_pdfScrute_1->setColumnCount ( header_pdfScrute.length () );
    ui->tableWidget_pdfScrute_1->setHorizontalHeaderLabels ( header_pdfScrute );
    ui->tableWidget_pdfScrute_1->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    QString VM =  ui->comboBox_VM_pdfScrute_1->currentText ();
    QString pathPDFScrute = config->getPathPDFScrute ();

    if( VM == "4AXES_35" )
        pathPDFScrute.replace ("$","");

    QSettings settings( "\\\\" % config->getAdrIP ( VM ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                        , QSettings::IniFormat );

    settings.beginGroup ("MUTUELLE");
    QStringList listeMutuelle = settings.allKeys ();

    //On met � jour le nombre de ligne
    ui->tableWidget_pdfScrute_1->setRowCount ( listeMutuelle.length () );

    //On boucle sur la liste des cl�s
    for( int i = 0 ; i < listeMutuelle.length () ; i++ ){

        QTableWidgetItem *itemActivite;
        QTableWidgetItem *itemMutuelle = new QTableWidgetItem( listeMutuelle.at (i) );

        //Ajout des donn�es dans la tableau
        ui->tableWidget_pdfScrute_1->setItem (i,0, itemMutuelle);
        ui->tableWidget_pdfScrute_1->item (i,0)->setTextAlignment (Qt::AlignCenter);

        //Condition sur l'acitivit� de la mutuelle [ 1 => ACTIVE / 0 => INACTIVE / Autre ou -1 => Inconnu ]
        if( settings.value (listeMutuelle.at (i), "-1").toString () == "1" ){

            itemActivite = new QTableWidgetItem( "Active" );
            itemActivite->setBackgroundColor ( Qt::green );
        }
        else if (settings.value (listeMutuelle.at (i), "-1").toString () == "0"){

            itemActivite = new QTableWidgetItem( "Inactive" );
            itemActivite->setBackgroundColor ( Qt::red );

        }
        else{

            itemActivite = new QTableWidgetItem( "Inconnu" );
            itemActivite->setBackgroundColor ( Qt::blue);
        }

        itemActivite->setFlags (Qt::ItemIsTristate);
        //Ajout des donn�es dans la tableau
        ui->tableWidget_pdfScrute_1->setItem (i,1, itemActivite);
        ui->tableWidget_pdfScrute_1->item (i,1)->setTextAlignment (Qt::AlignCenter);
    }
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Supprime un enregistrement du PDFScrute.ini
  */
void MainWindow::on_btn_supprimer_pdfscrute_1_clicked(){

    if( !ui->tableWidget_pdfScrute_1->rowCount () == 0 ){

        int resultat = QMessageBox::question (NULL,"Confirmation suppression","Souhaitez-cous vraiment supprimer la mutuelle [ "
                               % mutuelleSelectionnee_1 % " ] ? ", QMessageBox::Yes | QMessageBox::No);

        if( resultat == QMessageBox::Yes ){

            QString VM = ui->comboBox_VM_pdfScrute_1->currentText ();
            QString pathPDFScrute = config->getPathPDFScrute ();

            if( VM == "4AXES_35" )
                pathPDFScrute.replace ("$","");

            QSettings settings( "\\\\" % config->getAdrIP ( VM )
                                % "\\" % pathPDFScrute % "\\PDFScrute.ini", QSettings::IniFormat );

            settings.beginGroup ("MUTUELLE");

            settings.remove ( mutuelleSelectionnee_1 );

            if( !settings.contains ( mutuelleSelectionnee_1 ) ){

                settings.endGroup ();

                settings.beginGroup ("Spec");
                settings.remove (mutuelleSelectionnee_1);
                settings.endGroup ();

                QMessageBox::information (NULL,"Suppression","La valeur du PDFScrute.ini a �t� supprim�");
                ui->tableWidget_pdfScrute_1->removeRow ( itemSelect->row () );
            }
            else
                QMessageBox::warning (NULL,"Erreur suppression","Impossible de supprimer la valeur dans le PDFScrute.ini");
        }
    }
    else
        QMessageBox::warning (NULL,"Erreur suppression","Veuillez importer un PDFScrute");



}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Ajoute un enregistrement dans le PDFSCRUTE.ini
  */
void MainWindow::on_btn_ajouter_clicked(){

    if( !ui->tableWidget_pdfScrute_1->rowCount () == 0 ){

        ajouterPDFScrute *OBJ_ajouterDonnees = new ajouterPDFScrute( ui->comboBox_VM_pdfScrute_1->currentText () );
        connect (OBJ_ajouterDonnees, SIGNAL(succesInsert(QString,QString)), this, SLOT(ajouterLignePDFScrute_1(QString,QString)));
        OBJ_ajouterDonnees->show ();
    }
    else
        QMessageBox::warning (NULL,"Erreur ajout","Veuillez importer un PDFScrute");




}






//----------------------------------------------------------------------------------------------------------------------
void MainWindow::ajouterLignePDFScrute_1(QString mutuelle, QString activite){

    QTableWidgetItem *itemMutuelle = new QTableWidgetItem( mutuelle );
    QTableWidgetItem *itemActivite;

    if( activite == "1" ){

        itemActivite = new QTableWidgetItem( "Active" );
        itemActivite->setBackgroundColor ( Qt::green );
    }
    else if( activite == "0" ){

        itemActivite = new QTableWidgetItem( "Inactive" );
        itemActivite->setBackgroundColor ( Qt::red );
    }
    else{

        itemActivite = new QTableWidgetItem( "Inconnu" );
        itemActivite->setBackgroundColor ( Qt::blue );
    }

    ui->tableWidget_pdfScrute_1->insertRow (0);

    //Ajout des donn�es dans la tableau
    ui->tableWidget_pdfScrute_1->setItem (0,0, itemMutuelle);
    ui->tableWidget_pdfScrute_1->item (0,0)->setTextAlignment (Qt::AlignCenter);

    //Ajout des donn�es dans la tableau
    itemActivite->setFlags (Qt::ItemIsTristate);
    ui->tableWidget_pdfScrute_1->setItem (0,1, itemActivite);
    ui->tableWidget_pdfScrute_1->item (0,1)->setTextAlignment (Qt::AlignCenter);

}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Modifier" du PDFScrute 1
  */
void MainWindow::on_btn_modifier_pdfscrute_clicked(){

    if( !ui->tableWidget_pdfScrute_1->rowCount () == 0 ){

        modifierPDFScrute *OBJ_modifierDonnees = new modifierPDFScrute( ui->comboBox_VM_pdfScrute_1->currentText (),
                                                                        mutuelleSelectionnee_1, activiteMutuelle_1 );
        connect(OBJ_modifierDonnees, SIGNAL(succesEdit(QString)), this, SLOT(modifierLignePDFScrute_1(QString)));

        OBJ_modifierDonnees->show ();
    }
    else
        QMessageBox::warning (NULL,"Erreur modification","Veuillez importer un PDFScrute");



}



void MainWindow::modifierLignePDFScrute_1(QString activiteMutuelle){

    QTableWidgetItem *itemActivite;

    if(activiteMutuelle == "1" ){

        itemActivite = new QTableWidgetItem( "Active" );
        itemActivite->setBackgroundColor ( Qt::green );
    }
    else if(activiteMutuelle == "0" ){

        itemActivite = new QTableWidgetItem( "Inactive" );
        itemActivite->setBackgroundColor ( Qt::red );
    }
    else{

        itemActivite = new QTableWidgetItem( "Inconnu" );
        itemActivite->setBackgroundColor ( Qt::blue );
    }

    ui->tableWidget_pdfScrute_1->setItem ( itemSelect->row (), 1, itemActivite);

    itemActivite->setFlags (Qt::ItemIsTristate);
    ui->tableWidget_pdfScrute_1->item (itemSelect->row (),1)->setTextAlignment (Qt::AlignCenter);
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de basculer du PDFSCRUTE 1 au PDFSCRUTE 2
  */
void MainWindow::on_btn_basculer_1_clicked(){

    if( !ui->tableWidget_pdfScrute_1->rowCount () == 0 ){

        if( ui->tableWidget_pdfScrute_2->rowCount () != 0 ){

            modifierPDFScrute *OBJ_modifierDonnees;
            QPair<bool, QString> paire = OBJ_modifierDonnees->supprimerDonnees ( mutuelleSelectionnee_1, ui->comboBox_VM_pdfScrute_1->currentText () );

            if( paire.first != NULL && !paire.second.isEmpty () ){

                ajouterPDFScrute *OBJ_ajouterDonnees;
                OBJ_ajouterDonnees->ajouterDonnee ( mutuelleSelectionnee_1, paire.second, ui->comboBox_VM_pdfScrute_2->currentText () );

                QTableWidgetItem *itemActivite;
                QTableWidgetItem *itemMutuelle = new QTableWidgetItem( mutuelleSelectionnee_1);

                if( paire.second == "1" ){

                    itemActivite = new QTableWidgetItem( "Active" );
                    itemActivite->setBackgroundColor ( Qt::green );
                }
                else if (paire.second == "0" ){

                    itemActivite = new QTableWidgetItem( "Inactive" );
                    itemActivite->setBackgroundColor ( Qt::red );
                }
                else{

                    itemActivite = new QTableWidgetItem( "Inconnu" );
                    itemActivite->setBackgroundColor ( Qt::blue );
                }

                ui->tableWidget_pdfScrute_2->insertRow (0);

                //Ajout des donn�es dans la tableau
                ui->tableWidget_pdfScrute_2->setItem (0,0, itemMutuelle);
                ui->tableWidget_pdfScrute_2->item (0,0)->setTextAlignment (Qt::AlignCenter);

                //Ajout des donn�es dans la tableau
                itemActivite->setFlags (Qt::ItemIsTristate);
                ui->tableWidget_pdfScrute_2->setItem (0,1, itemActivite);
                ui->tableWidget_pdfScrute_2->item (0,1)->setTextAlignment (Qt::AlignCenter);

                ui->tableWidget_pdfScrute_1->removeRow ( itemSelect->row () );

            }
            else
                QMessageBox::warning (NULL,"Donn�es vide", "Les donn�es � basculer sont vides");
        }
        else
            QMessageBox::warning (NULL,"Importer PDFScrute", "Veuillez importer le seconde PDFScrute pour basculer les donn�es");
    }
    else
        QMessageBox::warning (NULL,"Erreur transfert","Veuillez importer un PDFScrute");


}




























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur effectue une recherche dans le PDFScrute 1
  */
void MainWindow::on_lineEdit_recherchePdfScrute_1_textChanged(const QString &arg1){

    for (int i = 0 ; i < ui->tableWidget_pdfScrute_1->rowCount() ; i++) {

            if(! ui->tableWidget_pdfScrute_1->item(i,0)->text().contains(arg1)){

                ui->tableWidget_pdfScrute_1->setRowHidden(i,true);
            }
            else
                ui->tableWidget_pdfScrute_1->setRowHidden(i,false);
    }
}































//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite obtenir que la liste des mutuelles actives
  */
void MainWindow::on_radioButton_active_1_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_1->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_1->setRowHidden(i,false);

         if( (ui->radioButton_active_1->isChecked () ) ){

            if(ui->tableWidget_pdfScrute_1->item(i,1)->text() == "Inactive")
                ui->tableWidget_pdfScrute_1->setRowHidden(i,true);
         }
         else
            ui->tableWidget_pdfScrute_1->setRowHidden(i,false);
   }
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite obtenir que la liste des mutuelles inactives
  */
void MainWindow::on_radioButton_inactive_1_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_1->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_1->setRowHidden(i,false);

         if( (ui->radioButton_inactive_1->isChecked () ) ){

            if(ui->tableWidget_pdfScrute_1->item(i,1)->text() ==  "Active")
                ui->tableWidget_pdfScrute_1->setRowHidden(i,true);
         }
         else
            ui->tableWidget_pdfScrute_1->setRowHidden(i,false);
   }

}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite obtenir que la liste des mutuelles compl�tes
  */
void MainWindow::on_radioButton_RAZ_1_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_1->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_1->setRowHidden(i,false);
    }
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet � l'utilisateur d'importer le PDFScrute
  */
void MainWindow::on_btn_importer_pdfscrute2_clicked(){

    //Initalisation tableau
    ui->tableWidget_pdfScrute_2->setColumnCount ( header_pdfScrute.length () );
    ui->tableWidget_pdfScrute_2->setHorizontalHeaderLabels ( header_pdfScrute );
    ui->tableWidget_pdfScrute_2->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    QString VM =  ui->comboBox_VM_pdfScrute_2->currentText ();

    QString pathPDFScrute = config->getPathPDFScrute ();

    if( VM == "4AXES_35" )
        pathPDFScrute.replace ("$","");

    QSettings settings( "\\\\" % config->getAdrIP ( VM ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                        , QSettings::IniFormat );

    settings.beginGroup ("MUTUELLE");
    QStringList listeMutuelle = settings.allKeys ();

    //On met � jour le nombre de ligne
    ui->tableWidget_pdfScrute_2->setRowCount ( listeMutuelle.length () );

    //On boucle sur la liste des cl�s
    for( int i = 0 ; i < listeMutuelle.length () ; i++ ){

        QTableWidgetItem *itemActivite;
        QTableWidgetItem *itemMutuelle = new QTableWidgetItem( listeMutuelle.at (i) );

        //Ajout des donn�es dans la tableau
        ui->tableWidget_pdfScrute_2->setItem (i,0, itemMutuelle);
        ui->tableWidget_pdfScrute_2->item (i,0)->setTextAlignment (Qt::AlignCenter);

        //Condition sur l'acitivit� de la mutuelle [ 1 => ACTIVE / 0 => INACTIVE / Autre ou -1 => Inconnu ]
        if( settings.value (listeMutuelle.at (i), "-1").toString () == "1" ){

            itemActivite = new QTableWidgetItem( "Active" );
            itemActivite->setBackgroundColor ( Qt::green );
        }

        else if (settings.value (listeMutuelle.at (i), "-1").toString () == "0"){

            itemActivite = new QTableWidgetItem( "Inactive" );
            itemActivite->setBackgroundColor ( Qt::red );

        }
        else{

            itemActivite = new QTableWidgetItem( "Inconnu" );
            itemActivite->setBackgroundColor ( Qt::blue);

        }

        itemActivite->setFlags (Qt::ItemIsTristate);
        //Ajout des donn�es dans la tableau
        ui->tableWidget_pdfScrute_2->setItem (i,1, itemActivite);
        ui->tableWidget_pdfScrute_2->item (i,1)->setTextAlignment (Qt::AlignCenter);
    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Ajouter" du PDFScrute 2
  */
void MainWindow::on_btn_ajouter_2_clicked(){

    if( !ui->tableWidget_pdfScrute_2->rowCount () == 0 ){

        ajouterPDFScrute *OBJ_ajouterDonnees = new ajouterPDFScrute( ui->comboBox_VM_pdfScrute_2->currentText () );
        connect (OBJ_ajouterDonnees, SIGNAL(succesInsert(QString,QString)), this, SLOT(ajouterLignePDFScrute_2(QString,QString)));
        OBJ_ajouterDonnees->show ();

    }
    else
        QMessageBox::warning (NULL,"Erreur ajout","Veuillez importer un PDFScrute");

}






//----------------------------------------------------------------------------------------------------------------------
void MainWindow::ajouterLignePDFScrute_2(QString mutuelle, QString activite){

    QTableWidgetItem *itemMutuelle = new QTableWidgetItem( mutuelle );
    QTableWidgetItem *itemActivite;

    if( activite == "1" ){

        itemActivite = new QTableWidgetItem( "Active" );
        itemActivite->setBackgroundColor ( Qt::green );
    }
    else if( activite == "0" ){

        itemActivite = new QTableWidgetItem( "Inactive" );
        itemActivite->setBackgroundColor ( Qt::red );
    }
    else{

        itemActivite = new QTableWidgetItem( "Inconnu" );
        itemActivite->setBackgroundColor ( Qt::blue );
    }

    ui->tableWidget_pdfScrute_2->insertRow (0);

    //Ajout des donn�es dans la tableau
    ui->tableWidget_pdfScrute_2->setItem (0,0, itemMutuelle);
    ui->tableWidget_pdfScrute_2->item (0,0)->setTextAlignment (Qt::AlignCenter);

    //Ajout des donn�es dans la tableau
    itemActivite->setFlags (Qt::ItemIsTristate);
    ui->tableWidget_pdfScrute_2->setItem (0,1, itemActivite);
    ui->tableWidget_pdfScrute_2->item (0,1)->setTextAlignment (Qt::AlignCenter);

}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Supprime un enregistrement du PDFScrute.ini
  */
void MainWindow::on_btn_supprimer_pdfscrute_2_clicked(){

    if( !ui->tableWidget_pdfScrute_2->rowCount () == 0 ){

        int resultat = QMessageBox::question (NULL,"Confirmation suppression","Souhaitez-cous vraiment supprimer la mutuelle [ "
                               % mutuelleSelectionnee_2 % " ] ? ", QMessageBox::Yes | QMessageBox::No);

        if( resultat == QMessageBox::Yes ){

            QString VM = ui->comboBox_VM_pdfScrute_2->currentText ();
            QString pathPDFScrute = config->getPathPDFScrute ();

            if( VM == "4AXES_35" )
                pathPDFScrute.replace ("$","");

            QSettings settings( "\\\\" % config->getAdrIP ( VM )
                                % "\\" % pathPDFScrute % "\\PDFScrute.ini", QSettings::IniFormat );

            settings.beginGroup ("MUTUELLE");

            settings.remove ( mutuelleSelectionnee_2 );

            if( !settings.contains ( mutuelleSelectionnee_2 ) ){

                settings.endGroup ();

                settings.beginGroup ("Spec");
                settings.remove (mutuelleSelectionnee_1);
                settings.endGroup ();

                QMessageBox::information (NULL,"Suppression","La valeur du PDFScrute.ini a �t� supprim�");
                ui->tableWidget_pdfScrute_2->removeRow ( itemSelect->row () );
            }
            else
                QMessageBox::warning (NULL,"Erreur suppression","Impossible de supprimer la valeur dans le PDFScrute.ini");

        }
    }
    QMessageBox::warning (NULL,"Erreur suppression","Veuillez importer un PDFScrute");
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Modifier" du PDFScrute 2
  */
void MainWindow::on_btn_modifier_pdfscrute_2_clicked(){

    if( !ui->tableWidget_pdfScrute_2->rowCount () == 0 ){

        modifierPDFScrute *OBJ_modifierDonnees = new modifierPDFScrute( ui->comboBox_VM_pdfScrute_2->currentText (),
                                                                        mutuelleSelectionnee_2, activiteMutuelle_2 );
        connect (OBJ_modifierDonnees, SIGNAL( succesEdit(QString) ), this, SLOT(modifierLignePDFScrute_2(QString) ) );
        OBJ_modifierDonnees->show ();
    }
    else
        QMessageBox::warning (NULL,"Erreur modification","Veuillez importer un PDFScrute");

}






void MainWindow::modifierLignePDFScrute_2(QString activiteMutuelle){

    QTableWidgetItem *itemActivite;

    if(activiteMutuelle == "1" ){

        itemActivite = new QTableWidgetItem( "Active" );
        itemActivite->setBackgroundColor ( Qt::green );
    }
    else if(activiteMutuelle == "0" ){

        itemActivite = new QTableWidgetItem( "Inactive" );
        itemActivite->setBackgroundColor ( Qt::red );
    }
    else{

        itemActivite = new QTableWidgetItem( "Inconnu" );
        itemActivite->setBackgroundColor ( Qt::blue );
    }

    ui->tableWidget_pdfScrute_2->setItem ( itemSelect->row (), 1, itemActivite);

    itemActivite->setFlags (Qt::ItemIsTristate);
    ui->tableWidget_pdfScrute_2->item (itemSelect->row (),1)->setTextAlignment (Qt::AlignCenter);
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de basculer du PDFSCRUTE 2 au PDFSCRUTE 1
  */
void MainWindow::on_btn_basculer_2_clicked(){

    if( !ui->tableWidget_pdfScrute_2->rowCount () == 0 ){

        if( ui->tableWidget_pdfScrute_1->rowCount () != 0 ){

            modifierPDFScrute *OBJ_modifierDonnees;
            QPair<bool, QString> paire = OBJ_modifierDonnees->supprimerDonnees ( mutuelleSelectionnee_2, ui->comboBox_VM_pdfScrute_2->currentText () );

            if( paire.first != NULL && !paire.second.isEmpty () ){

                ajouterPDFScrute *OBJ_ajouterDonnees;
                OBJ_ajouterDonnees->ajouterDonnee ( mutuelleSelectionnee_2, paire.second, ui->comboBox_VM_pdfScrute_1->currentText () );

                QTableWidgetItem *itemActivite;
                QTableWidgetItem *itemMutuelle = new QTableWidgetItem( mutuelleSelectionnee_2);

                if( paire.second == "1" ){

                    itemActivite = new QTableWidgetItem( "Active" );
                    itemActivite->setBackgroundColor ( Qt::green );
                }
                else if (paire.second == "0" ){

                    itemActivite = new QTableWidgetItem( "Inactive" );
                    itemActivite->setBackgroundColor ( Qt::red );
                }
                else{

                    itemActivite = new QTableWidgetItem( "Inconnu" );
                    itemActivite->setBackgroundColor ( Qt::blue );
                }

                ui->tableWidget_pdfScrute_1->insertRow (0);

                //Ajout des donn�es dans la tableau
                ui->tableWidget_pdfScrute_1->setItem (0,0, itemMutuelle);
                ui->tableWidget_pdfScrute_1->item (0,0)->setTextAlignment (Qt::AlignCenter);

                //Ajout des donn�es dans la tableau
                itemActivite->setFlags (Qt::ItemIsTristate);
                ui->tableWidget_pdfScrute_1->setItem (0,1, itemActivite);
                ui->tableWidget_pdfScrute_1->item (0,1)->setTextAlignment (Qt::AlignCenter);

                ui->tableWidget_pdfScrute_2->removeRow ( itemSelect->row () );

            }
            else
                QMessageBox::warning (NULL,"Donn�es vide", "Les donn�es � basculer sont vides");
        }
        else
            QMessageBox::warning (NULL,"Importer PDFScrute", "Veuillez importer le seconde PDFScrute pour basculer les donn�es");

    }
    else
        QMessageBox::warning (NULL,"Erreur transfert","Veuillez importer un PDFScrute");


}








//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur effectue une recherche dans le PDFScrute 2
  */
void MainWindow::on_lineEdit_recherchePdfScrute_2_textChanged(const QString &arg1){

    for (int i = 0 ; i < ui->tableWidget_pdfScrute_2->rowCount() ; i++) {

        if(! ui->tableWidget_pdfScrute_2->item(i,0)->text().contains(arg1)){

            ui->tableWidget_pdfScrute_2->setRowHidden(i,true);
        }
        else
            ui->tableWidget_pdfScrute_2->setRowHidden(i,false);
    }
}
























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_active_2_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_2->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_2->setRowHidden(i,false);

         if( (ui->radioButton_active_2->isChecked () ) ){

            if(ui->tableWidget_pdfScrute_2->item(i,1)->text() == "Inactive")
                ui->tableWidget_pdfScrute_2->setRowHidden(i,true);
         }
         else
            ui->tableWidget_pdfScrute_2->setRowHidden(i,false);
   }

}


























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_inactive_2_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_2->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_2->setRowHidden(i,false);

         if( (ui->radioButton_inactive_2->isChecked () ) ){

            if(ui->tableWidget_pdfScrute_2->item(i,1)->text() ==  "Active")
                ui->tableWidget_pdfScrute_2->setRowHidden(i,true);
         }
         else
            ui->tableWidget_pdfScrute_2->setRowHidden(i,false);
   }
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_RAZ_2_clicked(){

    for (int i = 0; i < ui->tableWidget_pdfScrute_2->rowCount(); ++i) {

        ui->tableWidget_pdfScrute_2->setRowHidden(i,false);
    }
}











// __  __          _____  _____ _____ _   _  _____   //
// |  \/  |   /\   |  __ \|  __ \_   _| \ | |/ ____| //
// | \  / |  /  \  | |__) | |__) || | |  \| | |  __  //
// | |\/| | / /\ \ |  ___/|  ___/ | | | . ` | | |_ | //
// | |  | |/ ____ \| |    | |    _| |_| |\  | |__| | //
// |_|  |_/_/    \_\_|    |_|   |_____|_| \_|\_____| //








//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_add_1_clicked(){


    if( VM_1.isEmpty() )
        QMessageBox::warning(NULL,"Ajout impossible","Veuillez importer le mapping correspondant pour ajouter une nouvelle ligne");

    else{

        Form_add_ligne *new_formulaire = new Form_add_ligne(VM_1, listeInfoMapping_1);
        new_formulaire->show();
    }
}































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_supprimer_clicked(){


    if( !VM_1.isEmpty() && !ligne.isEmpty() ){

        if (QMessageBox::Yes == QMessageBox::question(NULL, "Confirmation suppression",
                                                      "�tes-vous s�r de vouloir supprimer les informations du mapping de la "+ VM_1 +" ?",
                                                      QMessageBox::Yes|QMessageBox::No)){

            ui->tableWidget_1->removeRow(rowSelected);
            listeInfoMapping_1.removeAt(rowSelected);

            new_exportateurMapping->exporterToOut(VM_1, listeInfoMapping_1);
        }
    }
    else
        QMessageBox::warning(NULL,"Suppression impossible","Veuillez s�lectionner un enregistrement pour le supprimer");

}































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_modifier_clicked(){

    if( ! VM_1.isEmpty() && !ligne.isEmpty() ){

        QStringList ligneDecoupee = ligne.split(";");

        if(ligneDecoupee.length() == 5){

            Form_set_ligne *new_form_set_ligne = new Form_set_ligne(VM_1 ,NULL, ligneDecoupee.at(0), ligneDecoupee.at(1),
                                                                    ligneDecoupee.at(2), ligneDecoupee.at(3),
                                                                    ligneDecoupee.at(4) );
            new_form_set_ligne->show();
        }
        else{

            Form_set_ligne *new_form_set_ligne = new Form_set_ligne(VM_1, ligneDecoupee.at(0), ligneDecoupee.at(1),
                                                                    ligneDecoupee.at(2), ligneDecoupee.at(3),
                                                                    ligneDecoupee.at(4), ligneDecoupee.at(5) );
            new_form_set_ligne->show();
        }
    }
    else
        QMessageBox::warning(NULL,"Impossible de modifier","Veuillez s�lectionner un enregistrement pour le modifier");

}

























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_lineEdit_rechercher_mapping_1_textChanged(const QString &arg1){


    QStringList listData;
    listData.clear ();

    if( ui->radioButton_1->isChecked () ){

        listData = rechercheDansMapping(listeInfoMapping_1, arg1);
        ui->tableWidget_1->clear();
        initialiserTableau(ui->tableWidget_1, header.length (),listData.length() , header);
        remplirTableau (listData, ui->tableWidget_1);

    }
    else if( ui->radioButton_2->isChecked () ){

        listData = rechercheDansMapping(listeInfoMapping_2, arg1);

        ui->tableWidget_2->clear();
        initialiserTableau(ui->tableWidget_2, header.length (),listData.length() , header);
        remplirTableau (listData, ui->tableWidget_2);
    }
}






















//-------------------------------------------------------------------------------------------------------------------------
/*
  Permet de chercher dans la liste repr�sentant le mapping
  */
QStringList MainWindow::rechercheDansMapping(QStringList liste,QString info){

    QStringList resultat;
    for(int i = 0 ; i < liste.length () ; i++){

        if( liste.at (i).contains (info) )
            resultat.push_back (liste.at (i));
    }
    return(resultat);
}



























































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_importer_2_clicked(){

    VM_2 = ui->comboBox_2_VM->currentText();
    listeInfoMapping_2 = new_importateurMapping->importerDonnee(VM_2);
    remplirTableWidget_2(*ui->tableWidget_2);
}





























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_2_cellClicked(int row, int column){

    rowSelected = row;
    ligne = listeInfoMapping_2.at(row);
}































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_add_2_clicked(){


    if( VM_2.isEmpty() )
        QMessageBox::warning(NULL,"Ajout impossible","Veuillez importer le mapping correspondant pour ajouter une nouvelle ligne");

    else{

        Form_add_ligne *new_formulaire = new Form_add_ligne(VM_2, listeInfoMapping_2);
        new_formulaire->show();
    }
}






































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_supprimer_2_clicked(){

    if( !VM_2.isEmpty() && !ligne.isEmpty() ){

        if (QMessageBox::Yes == QMessageBox::question(NULL, "Confirmation suppression",
                                                      "�tes-vous s�r de vouloir supprimer les informations du mapping de la "+ VM_2+" ?",
                                                      QMessageBox::Yes|QMessageBox::No)){

            ui->tableWidget_2->removeRow(rowSelected);
            listeInfoMapping_2.removeAt(rowSelected);

            new_exportateurMapping->exporterToOut(VM_2, listeInfoMapping_2);
        }
    }
    else
        QMessageBox::warning(NULL,"Suppression impossible","Veuillez s�lectionner un enregistrement pour le supprimer");

}















































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_modifier_2_clicked(){

    if( ! VM_2.isEmpty() && !ligne.isEmpty() ){

        QStringList ligneDecoupee = ligne.split(";");

        if(ligneDecoupee.length() == 5){

            Form_set_ligne *new_form_set_ligne = new Form_set_ligne(VM_2, NULL, ligneDecoupee.at(0),
                                                                    ligneDecoupee.at(1), ligneDecoupee.at(2),
                                                                    ligneDecoupee.at(3), ligneDecoupee.at(4) );
            new_form_set_ligne->show();
        }
        else{

            Form_set_ligne *new_form_set_ligne = new Form_set_ligne(VM_2, ligneDecoupee.at(0), ligneDecoupee.at(1),
                                                                    ligneDecoupee.at(2), ligneDecoupee.at(3),
                                                                    ligneDecoupee.at(4), ligneDecoupee.at(5) );
            new_form_set_ligne->show();
        }
    }
    else
        QMessageBox::warning(NULL,"Impossible de modifier","Veuillez s�lectionner un enregistrement pour le modifier");

}





























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionParam_tres_triggered(){

    Parametre *new_parametre = new Parametre();
    new_parametre->show();
}















































































//----------------------------------------------------------------------------------------------------------------------
/*
  Mise � jour de la liste contenant les informations du mapping
  ";" => ignorer � OUI sinon NON
*/
void MainWindow::decouperInformation_1(){


    listeInfo_decoupee_1.clear();
    QStringList list_temp;

    for(int i = 0; i < listeInfoMapping_1.length() ; i++){

        list_temp.clear();
        list_temp =  listeInfoMapping_1.at(i).split(";");

        //ASTUCE
        if(list_temp.length() == 6){

            list_temp[0] = "Oui";
            listeInfoMapping_1[i].insert( 0, QString("Oui") );
        }
        else{

            list_temp.push_front("Non");
            listeInfoMapping_1[i].insert( 0, QString("Non;") );
        }

        for(int k = 0; k < list_temp.length() ; k++){

            listeInfo_decoupee_1.push_back( list_temp.at(k) );
        }
    }
}




























//----------------------------------------------------------------------------------------------------------------------
/*
  Mise � jour de la liste contenant les informations du mapping
  ";" => ignorer � OUI sinon NON
*/
void MainWindow::decouperInformation_2(){


    listeInfo_decoupee_2.clear();
    QStringList list_temp;

    for(int i = 0; i < listeInfoMapping_2.length() ; i++){

        list_temp.clear();
        list_temp =  listeInfoMapping_2.at(i).split(";");

        //ASTUCE
        if(list_temp.length() == 6){

            list_temp[0] = "Oui";
            listeInfoMapping_2[i].insert( 0, QString("Oui") );
        }
        else{

            list_temp.push_front("Non");
            listeInfoMapping_2[i].insert( 0, QString("Non;") );
        }

        for(int k = 0; k < list_temp.length() ; k++){

            listeInfo_decoupee_2.push_back( list_temp.at(k) );
        }
    }
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : ajout d'une VM
  */
void MainWindow::on_actionAjouter_V_M_triggered(){


    Ajouter_VM *new_VM = new Ajouter_VM();
    new_VM->show();
}































//----------------------------------------------------------------------------------------------------------------------
/*
    SLOT : Lorsque l'utilisateur clique sur "Quitter"
    */
void MainWindow::on_actionQuitter_triggered(){

    QCoreApplication::quit();
}




































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::addChild(QTreeWidgetItem *parent, QString niveau_un){

    QTreeWidgetItem *itm = new QTreeWidgetItem();

    itm->setText(0,niveau_un);
    parent->addChild(itm);


}































//----------------------------------------------------------------------------------------------------------------------
/*
  Importe tous les mappings pour les afficher sur le tableau TABLE_RECHERCHE
  */
void MainWindow::on_btn_importer_allMapping_clicked(){


    listeMapping.clear();

    Configuration *config = new Configuration();

    QString                 VM;
    QString                 ligneCourante;
    QString                 path;
    QString                 adrIP;

    QStringList             detailMapping;


    //Ouverture du fichier config.ini
    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
    settings.beginGroup("VM");

    QStringList liste_VM = settings.allKeys();

    for(int i = 0 ; i < liste_VM.length() ; i++){

        VM = liste_VM.at(i);
        adrIP = config->getAdrIP(VM);

        if( adrIP != "192.168.182.35" )
            path = "\\\\" + adrIP + "\\" + config->getPathMapping() + "\\mapping.csv";
        else
            path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax\\mapping.csv";



        QFile fichierEntree(path);
        if( ! fichierEntree.open(QIODevice::Text | QIODevice::ReadOnly) )
            QMessageBox::warning(NULL,"Ouverture fichier","Impossible d'ouvrir le fichier mapping de la "+ VM
                                 + " | " + fichierEntree.errorString () );
        else{

            QTextStream fluxEntrant(&fichierEntree);

            while( ! fluxEntrant.atEnd() ){

                detailMapping.clear();

                ligneCourante = fluxEntrant.readLine();

                if( !ligneCourante.isEmpty () ){

                    if( ! nonIgnorer(ligneCourante) ){

                        detailMapping.push_back( getRNM(ligneCourante) );
                        detailMapping.push_back( getNumFax(ligneCourante) );
                        detailMapping.push_back( VM);

                        listeMapping.push_back(detailMapping);
                    }
                }
            }
        }

        ui->table_recherche->clear();
        initialiserTableau(ui->table_recherche, 3,listeMapping.length() , header_table_recherche);
        remplirTableau(listeMapping, ui->table_recherche);
    }
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne TRUE si l'entit� est ignor�e
  Sinon FALSE
  */
bool MainWindow::nonIgnorer(QString ligne){

    if( ligne.mid(0,1) == ";" )
        return true;
    else
        return false;
}




//----------------------------------------------------------------------------------------------------------------------
void MainWindow::debugger(QString titre, QString message){

    QMessageBox::warning (NULL, titre, message);
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le RNM
  */
QString MainWindow::getRNM(QString ligne){

    QStringList ligneDecouppee = ligne.split(";");

    return(ligneDecouppee.at(1));
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le num�ro de fax
  */
QString MainWindow::getNumFax(QString ligne){

    return( ligne.split(";").at(0) );
}




























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque le texte change dans le champs de recherche
  */
void MainWindow::on_lineEdit_recheche_textChanged(const QString &arg1){


    QList<QStringList> listeRecherche;

    //Si on a cocher un crit�re de recherche
    if( !ui->radioButton_fax->isChecked() && !ui->radioButton_mutuelle->isChecked() )
        QMessageBox::warning(NULL,"Crit�re recherche","Veuillez s�lectionner un crit�re de recherche !");

    else{

        //Selon le crit�re choisit
        if( ui->radioButton_mutuelle->isChecked())
            listeRecherche = rechercherParRNM(arg1,0);

        else if( ui->radioButton_fax->isChecked() )
            listeRecherche = rechercherParFax(arg1,1);


        ui->table_recherche->clear();
        initialiserTableau(ui->table_recherche, 3,listeRecherche.length() , header_table_recherche);
        //On charge le tableau
        remplirTableau(listeRecherche, ui->table_recherche);
    }
}































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'effectuer une recherche RNM_NOMoC
  */
QList<QStringList> MainWindow::rechercherParRNM(QString rnm, int pos){

    QStringList detail;
    QList<QStringList> resultat;

    for(int i = 0 ; i < listeMapping.length() ; i++){

        if( listeMapping[i].at(pos).contains(rnm) ){

            detail.clear();

            detail.push_back(listeMapping[i][0]);
            detail.push_back(listeMapping[i][1]);
            detail.push_back(listeMapping[i][2]);

            resultat.push_back(detail);
        }
    }

    return(resultat);
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'effectuer une recherche par fax
  */
QList<QStringList> MainWindow::rechercherParFax(QString fax, int pos){

    QStringList detail;
    QList<QStringList> resultat;

    for(int i = 0 ; i < listeMapping.length() ; i++){

        if( listeMapping[i].at(pos).contains(fax) ){

            detail.clear();

            detail.push_back(listeMapping[i][0]);
            detail.push_back(listeMapping[i][1]);
            detail.push_back(listeMapping[i][2]);

            resultat.push_back(detail);
        }
    }
    return(resultat);
}




























//----------------------------------------------------------------------------------------------------------------------
/*
    Remplit le tableau TABLE_RECHERCHE
    Proc�dure
    Param�tre entr�e : QListe<QStringList>
    */
void MainWindow::remplirTableau(QList<QStringList> listeDetail, QTableWidget *table){


    for(int j = 0 ; j < listeDetail.length() ; j++){

        for(int k = 0 ; k < listeDetail.at(j).length() ; k++){

            //Cr�ation d'une celulle contenant l'information de la table de concordance
            QTableWidgetItem *cellC = new QTableWidgetItem(listeDetail[j][k]);

            table->setItem(j,k,cellC);

            //Ajustement cellule
            cellC->setTextAlignment(Qt::AlignCenter);
            cellC->setTextColor("black");
        }
    }
}































//----------------------------------------------------------------------------------------------------------------------
/*
    SURCHARGE
    Remplit le tableau TABLE_RECHERCHE
    Proc�dure
    Param�tre entr�e : QListe<QStringList>
    */
void MainWindow::remplirTableau(QStringList listeDetail, QTableWidget *table){

    QStringList data;
    for(int j = 0 ; j < listeDetail.length() ; j++){

        data = listeDetail.at (j).split (";");

        for(int i = 0 ; i < data.length () ; i++){

            //Cr�ation d'une celulle contenant l'information de la table de concordance
            QTableWidgetItem *cellC = new QTableWidgetItem(data[i]);
            cellC->setFlags(Qt::ItemIsEditable);

            table->setItem(j,i,cellC);

            //Ajustement cellule
            cellC->setTextAlignment(Qt::AlignCenter);
            cellC->setTextColor("black");
        }
         data.clear ();
    }
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Boutin R-initialiser
    Recharge le tableau
*/
void MainWindow::on_btn_initialiser_clicked(){

    remplirTableau(listeMapping, ui->table_recherche);
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Permet de lister les dossiers des mutuelles de la VM
*/
void MainWindow::on_btn_lister_clicked(){

    QString VM = ui->comboBox_VM_TEMP->currentText ();
    QString adrIP = config->getAdrIP (VM);
    QString path;

//    QString path = "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\GoodNIN";

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\GoodNIN";
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax\\GoodNIN";


//    QString path = config->getPathPoper () + "\\GoodNIN";

    QStringList listeRepertoire;

    QDirIterator dirIterator(path, QDir::Dirs | QDir::NoDotAndDotDot);

    // Tant qu'on n'est pas arriv� � la fin de l'arborescence...
    while(dirIterator.hasNext()){

        listeRepertoire.push_back (QDir(dirIterator.next()).dirName ());
    }

    initialiserTableau (ui->table_OC, 1 ,listeRepertoire.length() , header_table_oc);

    for(int j = 0 ; j < listeRepertoire.length() ; j++){

        //Cr�ation d'une celulle contenant l'information de la table de concordance
        QTableWidgetItem *cellC = new QTableWidgetItem(listeRepertoire.at(j));

        ui->table_OC->setItem(0,j,cellC);

        //Ajustement cellule
        cellC->setTextAlignment(Qt::AlignCenter);
        //cellC->setFlags(Qt::ItemIsTristate);
        cellC->setTextColor("black");
    }
}




























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_table_OC_itemClicked(QTableWidgetItem *item){


    ui->btn_creerTEMP->setVisible (true);
    ui->lineEdit_newTEMP->setVisible (true);
    ui->label__ex_tmp->setVisible (true);
    itemSelectionne_TEMP = item->text ();

}























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Cr�ation de TMP.

  */
void MainWindow::on_btn_creerTEMP_clicked(){

    QString TMP = ui->lineEdit_newTEMP->text ();

    if( TMP.isEmpty () )
        QMessageBox::warning (NULL,"Champ vide","Veuillez indiquer un nom de dossier TMP");
    else{

        QStringList champTMP = TMP.split ("_");

        if( champTMP.length () < 3)
            QMessageBox::warning (NULL,"TMP invalide","Le dossier doit �tre de la forme TMP_xxxxxxxxx_yyyyyyyyy au minimum");

        else if(champTMP.at (0) != "TMP")
            QMessageBox::warning (NULL,"TMP invalide","Le nom du dossier doit commencer par TMP");

        else if( champTMP.at (1).length () != 9 &&  champTMP.at (1).length () != 10)
            QMessageBox::warning (NULL,"RNM invalide","Veuillez inscrire un RNM de 9 ou 10 caract�res !");

        else{

            champTMP.removeAt (0);
            TMP = champTMP.join ("_").toUpper ();

            QDir                dossier;
            QString adrIP = config->getAdrIP (ui->comboBox_VM_TEMP->currentText ());
            QString path = "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\GoodNIN\\" + itemSelectionne_TEMP + "\\ERREUR";

            dossier.mkdir ( path + "\\" + "TMP_" + TMP );

            path = "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\UnknownNIN\\" + itemSelectionne_TEMP;
            dossier.mkdir ( path + "\\" + "TMP_" + TMP );

            if( ! dossier.exists ( "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\GoodNIN\\" + ( TMP ) ) ){

                creerDossierGoodNIN( "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\GoodNIN\\" , dossier,  TMP ) ;
                creerDossierUnknownNIN( "\\\\" + adrIP + "\\" + config->getPathPoper () + "\\UnknownNIN\\", dossier,  TMP );

                QMessageBox::information (NULL,"Op�ration termin�e","Le TMP "+ TMP + " a �t� cr��");

                ui->lineEdit_newTEMP->clear ();
            }
            else
                QMessageBox::information (NULL,"Op�ration annul�e","Le TMP "+ TMP + " existe d�j� !");
        }
    }
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de cr�er le dossier de l'organisme compl�mentaire dans GOODNIN ainsi que les sous-dossiers
  */
void MainWindow::creerDossierGoodNIN (QString path, QDir dossier, QString mutuelle){

    dossier.mkdir ( path + mutuelle);

    dossier.mkdir (  path + mutuelle + "\\OK" );
    dossier.mkdir (  path + mutuelle + "\\ArchivesDPEC" );
    dossier.mkdir (  path + mutuelle + "\\ERREUR" );

    dossier.mkdir (  path + mutuelle + "\\ERREUR\\NR" );
    dossier.mkdir (  path + mutuelle + "\\ERREUR\\OK_MANUEL" );

}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de cr�er le dossier de l'organisme compl�mentaire dans UNKNOWNNIN ainsi que les sous-dossiers
    */
void MainWindow::creerDossierUnknownNIN (QString path, QDir dossier, QString mutuelle){

    dossier.mkdir ( path + mutuelle );
    dossier.mkdir (  path + mutuelle + "\\NR" );
    dossier.mkdir (  path + mutuelle + "\\OK_MANUEL" );
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  Charge les donn�es dans IHM
*/
void MainWindow::chargerDonneeIHM (QStringList liste){


    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 4, header_table_oc_gd_uk);

    for(int j = 0 ; j < liste.length () ; j++){

        //Cr�ation d'une celulle contenant l'information de la table de concordance
        QTableWidgetItem *cellC = new QTableWidgetItem(liste.at(j));

        ui->table_oc_gd_uk->setItem(0,j,cellC);

        //Ajustement cellule
        cellC->setTextAlignment(Qt::AlignCenter);
        cellC->setFlags(Qt::ItemIsTristate);
        cellC->setTextColor("black");
    }
    ui->table_oc_gd_uk->sortItems (3, Qt::DescendingOrder);
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton ERREUR
  */
void MainWindow::on_btn_aReconcilier_clicked(){

    QString VM = ui->comboBox_VM_Volume->currentText ();
    QString adrIP = config->getAdrIP (VM);
    QString path;

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper ();
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax";



    QStringList liste;
    liste.clear ();

    Trieur *new_trieur = new Trieur( path + "\\GoodNIN\\", path + "\\UnknownNIN\\");


    if( ui->radioButton_tout->isChecked () )
        liste = new_trieur->trierErreur (ui->lineEdit_total);

    else if( ui->radioButton_journee->isChecked () )
        liste = new_trieur->trierErreurDate (dateSelectionnee, ui->lineEdit_total);


    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 3, header_table_oc_gd_uk);
    chargerDonneeIHM(liste);
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton OK_MANUEL
  */
void MainWindow::on_btn_OK_MANUEL_clicked(){

    QString VM = ui->comboBox_VM_Volume->currentText ();
    QString adrIP = config->getAdrIP (VM);    
    QString path;

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper ();
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax";

    QStringList liste;

    Trieur *new_trieur = new Trieur( path + "\\GoodNIN", path + "\\UnknownNIN");

    if( ui->radioButton_tout->isChecked () )
        liste = new_trieur->trierOK_MANUEL (ui->lineEdit_total);

    else if( ui->radioButton_journee->isChecked () )
        liste = new_trieur->trierOK_MANUELDate (dateSelectionnee, ui->lineEdit_total);


    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 3, header_table_oc_gd_uk);

    chargerDonneeIHM(liste);
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton API
  */
void MainWindow::on_btn_API_clicked(){

    QString VM = ui->comboBox_VM_Volume->currentText ();
    QString adrIP = config->getAdrIP (VM);
    QString path;

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper ();
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax";

    QStringList liste;

    Trieur *new_trieur = new Trieur( path + "\\GoodNIN", path + "\\UnknownNIN");

    if( ui->radioButton_tout->isChecked () )
        liste = new_trieur->trierAPI (ui->lineEdit_total);

    else if( ui->radioButton_journee->isChecked() )
        liste = new_trieur->trierAPIDate (dateSelectionnee, ui->lineEdit_total);


    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 3, header_table_oc_gd_uk);

    chargerDonneeIHM(liste);
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton NR
  */
void MainWindow::on_btn_NR_clicked(){

    QString VM = ui->comboBox_VM_Volume->currentText ();
    QString adrIP = config->getAdrIP (VM);
    QString path;

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper ();
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax";
    QStringList liste;

    Trieur *new_trieur = new Trieur( path + "\\GoodNIN", path + "\\UnknownNIN");

    if( ui->radioButton_tout->isChecked () )
        liste = new_trieur->trierNR (ui->lineEdit_total);

    else if( ui->radioButton_journee->isChecked () )
        liste = new_trieur->trierNRDate (dateSelectionnee,ui->lineEdit_total);

    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 3, header_table_oc_gd_uk);

    chargerDonneeIHM(liste);
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton R�ceptionn�
  */
void MainWindow::on_btn_receptionne_clicked(){

    QString VM = ui->comboBox_VM_Volume->currentText ();
    QString adrIP = config->getAdrIP (VM);
    QString path;

    if( adrIP != "192.168.182.35" )
        path = "\\\\" + adrIP + "\\" + config->getPathPoper ();
    else
        path = "\\\\" + adrIP + "\\" + "c\\Users\\4axes\\Desktop\\PoperFax";

    QStringList liste;

    Trieur *new_trieur = new Trieur( path + "\\GoodNIN", path + "\\UnknownNIN");

    if( ui->radioButton_tout->isChecked () )
        liste = new_trieur->trierRecv (ui->lineEdit_total);

    initialiserTableau (ui->table_oc_gd_uk, 4, liste.length () / 3, header_table_oc_gd_uk);

    chargerDonneeIHM(liste);
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le calendrier
  */
void MainWindow::on_calendarWidget_clicked(const QDate &date){

//    if( ui->radioButton_periode->isChecked () ){

//        if(clickID == 0){

//            dateSelectionnee = date;
//            ui->lineEdit_date1->setText (dateSelectionnee.toString ("dd/MM/yyyy"));
//            clickID = 1;
//            dateSelectionnee_p1 = date;

//        }
//        else if(clickID == 1){

//            dateSelectionnee = date;
//            ui->lineEdit_date2->setText (dateSelectionnee.toString ("dd/MM/yyyy"));
//            clickID = 0;
//            dateSelectionnee_p2 = date;
//        }
//    }

    if( ui->radioButton_journee->isChecked () ){

        dateSelectionnee = date;
        ui->lineEdit_date1->setText (dateSelectionnee.toString ("dd/MM/yyyy"));
    }
}

















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_tout_clicked(){

    ui->lineEdit_date1->setVisible (false);
    ui->lineEdit_date2->setVisible (false);
    ui->btn_raz->setVisible (false);
}




















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_journee_clicked(){

    QDate dateJour;
    ui->lineEdit_date1->setText (dateJour.currentDate ().toString ("dd/MM/yyyy"));
    dateSelectionnee = dateJour.currentDate ();

    ui->btn_raz->setVisible (false);
    ui->lineEdit_date1->setVisible (true);
    ui->lineEdit_date2->setVisible (false);
}

















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_periode_clicked(){

    ui->btn_raz->setVisible (true);
    ui->lineEdit_date1->setVisible (true);
    ui->lineEdit_date2->setVisible (true);

}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_raz_clicked(){

    ui->lineEdit_date1->clear ();
    ui->lineEdit_date2->clear ();

    clickID = 0;
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur la celulle contenant le nom de la mutuelle
  */
void MainWindow::on_tableWidget_pdfScrute_1_cellClicked(int row, int column){

    mutuelleSelectionnee_1 = ui->tableWidget_pdfScrute_1->item (row, column)->text ();
    activiteMutuelle_1 = ui->tableWidget_pdfScrute_1->item (row, column + 1)->text ();
}




//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_pdfScrute_2_cellClicked(int row, int column){

    mutuelleSelectionnee_2 = ui->tableWidget_pdfScrute_2->item (row, column)->text ();
    activiteMutuelle_2 = ui->tableWidget_pdfScrute_2->item (row, column + 1)->text ();

}




//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_pdfScrute_1_itemClicked(QTableWidgetItem *item){

    itemSelect = item;
}




//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_pdfScrute_2_itemClicked(QTableWidgetItem *item){

    itemSelect = item;
}







////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////// PASSE DROIT////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////








//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite importer les passes droit des mutuelles depuis un PDFScrute d'une VM s�lectionn�e
  */
void MainWindow::on_btn_importerPasseDroit_clicked(){

    //Initalisation tableau
    ui->tableWidget_passeDroit->setColumnCount ( header_oc_passe_droit.length () );
    ui->tableWidget_passeDroit->setHorizontalHeaderLabels ( header_oc_passe_droit );
    ui->tableWidget_passeDroit->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    QString VM =  ui->comboBox_passeDroit->currentText ();
    QString pathPDFScrute = config->getPathPDFScrute ();

    if( VM == "4AXES_35" )
        pathPDFScrute.replace ("$","");

    QSettings settings( "\\\\" % config->getAdrIP ( VM ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                        , QSettings::IniFormat );

    settings.beginGroup ("Spec");
    QStringList listePasseDroit = settings.allKeys ();

    //On supprime la cl� "Popper" car elle n'est plus utilis�e
    listePasseDroit.removeAt ( listePasseDroit.indexOf ("Popper") );

    //On met � jour le nombre de ligne
    ui->tableWidget_passeDroit->setRowCount ( listePasseDroit.length () );

    //On boucle sur la liste des cl�s
    for( int i = 0 ; i < listePasseDroit.length () ; i++ ){

        QTableWidgetItem *itemMutuelle = new QTableWidgetItem( listePasseDroit.at (i) );
        QTableWidgetItem *itemPasseDroit = new QTableWidgetItem( settings.value (listePasseDroit.at (i)).toString ());

        //Ajout des donn�es dans la tableau
        ui->tableWidget_passeDroit->setItem (i,0, itemMutuelle);
        ui->tableWidget_passeDroit->item (i,0)->setTextAlignment (Qt::AlignCenter);

        itemPasseDroit->setFlags (Qt::ItemIsTristate);
        //Ajout des donn�es dans la tableau
        ui->tableWidget_passeDroit->setItem (i,1, itemPasseDroit);
        ui->tableWidget_passeDroit->item (i,1)->setTextAlignment (Qt::AlignCenter);
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur un item de la table de passe droit
  */
void MainWindow::on_tableWidget_passeDroit_itemClicked(QTableWidgetItem *item){

    itemSelect = item;
    mutuelleSelectPasseDroit = item->text ();
    ui->btn_modifierPasseDroit->setVisible ( true );
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Modifier" du tableau passe droit
  */
void MainWindow::on_btn_modifierPasseDroit_clicked(){

    modifierPasseDroit *OBJ_modifierPasseDroit = new modifierPasseDroit( ui->comboBox_passeDroit->currentText (),
                                                                         mutuelleSelectPasseDroit,
                                                                         ui->tableWidget_passeDroit->item ( itemSelect->row (), 1)->text () );
    connect( OBJ_modifierPasseDroit, SIGNAL( succesEditPasseDroit(QString) ), this, SLOT(modifierPassDroit(QString) ) );

    OBJ_modifierPasseDroit->show ();

}
















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : r�ception du sigal apr�s modification des passes droits
  */
void MainWindow::modifierPassDroit(QString passeDroit){

    QTableWidgetItem *itemPasseDroit = new QTableWidgetItem(passeDroit);

    ui->tableWidget_passeDroit->setItem ( itemSelect->row (), 1, itemPasseDroit);

    itemPasseDroit->setFlags (Qt::ItemIsTristate);
    ui->tableWidget_passeDroit->item (itemSelect->row (),1)->setTextAlignment (Qt::AlignCenter);
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_table_recherche_cellDoubleClicked(int row, int column){

    QDesktopServices::openUrl(QUrl("\\\\" % config->getAdrIP ( ui->table_recherche->item (row,2)->text () )
                                   % "\\" % config->getPathPoper () % "\\GoodNIN"
                                   % "\\" % ui->table_recherche->item (row,0)->text ()
                                   % "\\ERREUR", QUrl::TolerantMode));
}















/*

  / ____| | |           | |           |  __ \
 | (___   | |_    __ _  | |_   ___    | |__) |   ___    _ __    _ __     ___   _ __
  \___ \  | __|  / _` | | __| / __|   |  ___/   / _ \  | '_ \  | '_ \   / _ \ | '__|
  ____) | | |_  | (_| | | |_  \__ \   | |      | (_) | | |_) | | |_) | |  __/ | |
 |_____/   \__|  \__,_|  \__| |___/   |_|       \___/  | .__/  | .__/   \___| |_|
                                                       | |     | |
                                                       */



//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une date du calendrier
  */
void MainWindow::on_calendarWidget_rchPopper_clicked(const QDate &date){

    if( ui->checkBox_rchJournee->isChecked () | ui->checkBox_rchPeriode->isChecked () ){

        //Si journ�e activ�e
        if( ui->checkBox_rchJournee->isChecked () )
            ui->lineEdit_rchDateDebut->setText ( date.toString ("dd/MM/yyyy") );

        //Si p�riode activ�e
        else if( ui->checkBox_rchPeriode->isChecked () ){

            //Si aucun clique n'a �t� fait
            if( clickID == 0 ){

                ui->lineEdit_rchDateDebut->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 1;
            }
            //Lorsqu'un clique a d�j� �t� fait
            else if( clickID == 1 ){

                ui->lineEdit_rchDateFin->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 0;
            }
        }
    }
    else
        debugger ("S�lection crit�re","Veuillez s�lectionner une journ�e ou une p�riode avant de s�lectionner une date");
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le checkBox "Journ�e"
  */
void MainWindow::on_checkBox_rchJournee_clicked(){

    ui->checkBox_rchPeriode->setChecked ( false );
    ui->lineEdit_rchDateFin->setVisible ( false );
    ui->label_rchAu->setVisible ( false );
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le checkBox "P�riode"
  */
void MainWindow::on_checkBox_rchPeriode_clicked(){

    ui->checkBox_rchJournee->setChecked ( false );
    ui->lineEdit_rchDateFin->setVisible ( true );
    ui->label_rchAu->setVisible ( true );
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Importer"
  */
void MainWindow::on_btn_importerStatsPopper_clicked(){

    ui->tableWidget_rchPopper->setColumnCount ( headerStatsPopper.length () );
    ui->tableWidget_rchPopper->setHorizontalHeaderLabels ( headerStatsPopper );
    ui->tableWidget_rchPopper->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    QString vm = ui->comboBox_VM_rchStatsPopper->currentText ();
    QString pathPopper = config->getPathPoper ();

    if( vm == "4AXES_35" )
        pathPopper = config->getValue ( PATH_FICHIER_CONFIG, "path", "repoPoperx35");

    QString pathBDD = "\\\\" % config->getAdrIP (vm) % "\\" % pathPopper % "\\" % NOM_BDD_STATS_POPPER;

    listeStats.clear ();

    Dial_bdd *bdd = new Dial_bdd( pathBDD );
    bdd->importerTraceFax ( listeStats,
                                         ui->checkBox_rchJournee->isChecked (),
                                         ui->lineEdit_rchDateDebut->text (),
                                         ui->lineEdit_rchDateFin->text () );

    ajouterLignesIHM ( listeStats, ui->tableWidget_rchPopper );

}













//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ajouter une ligne sur le tableau d'affichage statistique popper
  */
void MainWindow::ajouterLignesIHM(QList<QStringList> liste, QTableWidget *table){

    table->setRowCount ( liste.length () );

    for(int i = 0 ; i < liste.length () ; i++){

        for(int j = 0 ; j < liste.at (i).length () ; j++){

            //Cr�ation d'une celulle contenant l'information de la table de concordance
            QTableWidgetItem *cellule = new QTableWidgetItem( liste.at (i).at (j) );
            table->setItem(i,j,cellule);

            cellule->setTextAlignment(Qt::AlignCenter);
            cellule->setTextColor("black");
        }
    }
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur tape un mot cl� dans la zone de recherche
  */
void MainWindow::on_lineEdit_rechercherMutuelle_textChanged(const QString &arg1){

    //Si on a cocher un crit�re de recherche
    if( !ui->radioButton_rchMutuelle->isChecked() && !ui->radioButton_rchNumFax->isChecked() )
        debugger ("Crit�re recherche","Veuillez s�lectionner un crit�re de recherche !");

    else{

        QList<QStringList> listeResultats;
        QStringList resultat;

        for(int i = 0 ; i < listeStats.length () ; i++){

            if( ui->radioButton_rchMutuelle->isChecked () ){

                if( listeStats.at (i).at (1).contains ( arg1 ) ){

                    resultat.clear ();

                    resultat.push_back ( listeStats.at (i).at (0) );
                    resultat.push_back ( listeStats.at (i).at (1) );
                    resultat.push_back ( listeStats.at (i).at (2) );

                    listeResultats.push_back ( resultat );
                }
            }
            else if ( ui->radioButton_rchNumFax ){


                if( listeStats.at (i).at (0).contains ( arg1 ) ){

                    resultat.clear ();

                    resultat.push_back ( listeStats.at (i).at (0) );
                    resultat.push_back ( listeStats.at (i).at (1) );
                    resultat.push_back ( listeStats.at (i).at (2) );

                    listeResultats.push_back ( resultat );
                }
            }
        }

        ajouterLignesIHM ( listeResultats, ui->tableWidget_rchPopper );
    }
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer la base de donn�es "Base_stats" de chaque VM
  */
void MainWindow::on_btn_importeBaseStats_clicked(){

    ui->tableWidget_infoReconciliation->setColumnCount ( headerRechercherRecon.length () );
    ui->tableWidget_infoReconciliation->setHorizontalHeaderLabels ( headerRechercherRecon );
    ui->tableWidget_infoReconciliation->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    QString ip;
    QString path;
    QString pathBDD;

    ip = config->getAdrIP ( ui->comboBox_VM_detailReconciliation->currentText () );
    path = config->getValue ( PATH_FICHIER_CONFIG, "Path", "apiManuelle");

    if( path == "-1")
        debugger ("Configuration","Veuillez v�rifier la valeur de la cl� [apiManuelle] dans le groupe [ Path ] ");
    else{

        listeInfosReconciliation.clear ();

        pathBDD = "\\\\" % ip % "\\" % path % "\\" % NOM_BDD_RECONCILIATION;

        Dial_bdd *bdd = new Dial_bdd( pathBDD );
        bdd->importerDetailReconciliation ( listeInfosReconciliation, ui->comboBox_VM_detailReconciliation->currentText () );

        ajouterLignesIHM ( listeInfosReconciliation, ui->tableWidget_infoReconciliation);
    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite effectuer une recherche
  */
void MainWindow::on_lineEdit_rechercheInfoReconciliation_textChanged(const QString &arg1){

    if( ui->checkBox_nomFichier->isChecked () || ui->checkBox_nomPatient->isChecked () || ui->checkBox_ch->isChecked () ){

        int index = 0;

        if( ui->checkBox_nomPatient->isChecked () )
            index = 6;
        else if ( ui->checkBox_ch->isChecked () )
            index = 10;

        afficherRecherche (index, arg1, "-1");

    }
    else
        debugger ("S�lection recherche","Veuillez s�lectionner un crit�re de recherche");
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le checkBox "Journ�e" de l'onglet "Recherche r�conciliation"
  */
void MainWindow::on_checkBox_journeeReconciliation_clicked(bool checked){

    ui->checkBox_periodeReconciliation->setChecked ( false );
    ui->checkBox_periodeReconciliation->setVisible ( !checked );
    ui->lineEdit_dateFinReconciliation->setVisible ( !checked );
    ui->lineEdit_dateDebutReconciliation->setText ( QDate::currentDate ().toString ("dd/MM/yyyy") );

    if( !checked )
        ui->lineEdit_dateDebutReconciliation->clear ();
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le checkBox "P�riode" de l'onglet "Recherche r�conciliation"
  */
void MainWindow::on_checkBox_periodeReconciliation_clicked(bool checked){

    ui->checkBox_journeeReconciliation->setChecked (false);
    ui->checkBox_journeeReconciliation->setVisible ( !checked );
    ui->lineEdit_dateFinReconciliation->setVisible ( checked );
    ui->lineEdit_dateDebutReconciliation->clear ();
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une date du calendrier
*/
void MainWindow::on_calendrierReconciliation_clicked(const QDate &date){

    if( ui->checkBox_journeeReconciliation->isChecked () | ui->checkBox_periodeReconciliation->isChecked () ){

        //Si journ�e activ�e
        if( ui->checkBox_journeeReconciliation->isChecked () )
            ui->lineEdit_dateDebutReconciliation->setText ( date.toString ("dd/MM/yyyy") );

        //Si p�riode activ�e
        else if( ui->checkBox_periodeReconciliation->isChecked () ){

            //Si aucun clique n'a �t� fait
            if( clickID == 0 ){

                ui->lineEdit_dateDebutReconciliation->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 1;
            }
            //Lorsqu'un clique a d�j� �t� fait
            else if( clickID == 1 ){

                ui->lineEdit_dateFinReconciliation->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 0;
            }
        }
    }
    else
        debugger ("S�lection crit�re","Veuillez s�lectionner une journ�e ou une p�riode avant de s�lectionner une date");
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Filtrer"
  */
void MainWindow::on_btn_filtreDate_clicked(){

    //V�rifier si un de deux checkBox
    if( ui->checkBox_journeeReconciliation->isChecked () || ui->checkBox_periodeReconciliation->isChecked () ){

        if( ui->checkBox_journeeReconciliation->isChecked () ){

            if( !ui->lineEdit_dateDebutReconciliation->text ().isEmpty () )
                afficherRecherche(1, QDate::fromString ( ui->lineEdit_dateDebutReconciliation->text ()
                                                         , "dd/MM/yyyy").toString ("yyyyMMdd"),"-1");

            else
                debugger ("S�lection date","Veuillez s�lectionner une date pour votre crit�re de recherche");
        }
        else if( ui->checkBox_periodeReconciliation->isChecked () ){

            if( !ui->lineEdit_dateDebutReconciliation->text ().isEmpty () && !ui->lineEdit_dateFinReconciliation->text ().isEmpty () ){

                afficherRecherche(1, QDate::fromString ( ui->lineEdit_dateDebutReconciliation->text (), "dd/MM/yyyy").toString ("yyyyMMdd"),
                                  QDate::fromString ( ui->lineEdit_dateFinReconciliation->text (), "dd/MM/yyyy").toString ("yyyyMMdd") );

            }
            else
                debugger ("S�lection date","Veuillez s�lectionner une p�riode pour votre crit�re de recherche");
        }

    }
    else
        debugger ("S�lection crit�re","Veuillez s�lectionner un crit�re de date pour filtrer votre recherche");
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'effectuer une recherche par journ�e/p�riode
  */
void MainWindow::afficherRecherche(int indexRecherche, QString dateDebut, QString dateFin){

    int nbResultat = 0;

    if( dateFin == "-1" ){

        for (int i = 0 ; i < ui->tableWidget_infoReconciliation->rowCount() ; i++) {

            if( ! ui->tableWidget_infoReconciliation->item(i,indexRecherche)->text().contains( dateDebut ) )
                ui->tableWidget_infoReconciliation->setRowHidden(i,true);
            else{

                ui->tableWidget_infoReconciliation->setRowHidden(i,false);
                nbResultat++;
            }
        }
    }
    else{

        for (int i = 0 ; i < ui->tableWidget_infoReconciliation->rowCount() ; i++) {

            if( ui->tableWidget_infoReconciliation->item(i,indexRecherche)->text() >= dateDebut &&
                    ui->tableWidget_infoReconciliation->item(i,indexRecherche)->text() <= dateFin ){

                ui->tableWidget_infoReconciliation->setRowHidden(i,false);
                nbResultat++;
            }
            else
                ui->tableWidget_infoReconciliation->setRowHidden(i,true);
        }
    }
    afficherNombreResultat ( QString::number ( nbResultat) );
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'effectuer une recherche par mot cl�
  */
void MainWindow::afficherRecherche(int indexRecherche, QString motCle){

    int nbResultat = 0;

    for (int i = 0 ; i < ui->tableWidget_infoReconciliation->rowCount() ; i++) {

        if( ! ui->tableWidget_infoReconciliation->item(i,indexRecherche)->text().contains( motCle.toUpper () ) )
            ui->tableWidget_infoReconciliation->setRowHidden(i,true);
        else{

            ui->tableWidget_infoReconciliation->setRowHidden(i,false);
            nbResultat++;
        }
    }
    afficherNombreResultat ( QString::number ( nbResultat) );
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Affiche sur l'IHM le nombre de r�sultat
  */
void MainWindow::afficherNombreResultat(QString nbResultat){

    ui->label_nbResultat->setText ( "Nombre de r�sultat : " % nbResultat );
}







/*
 _____  _        _   _     _   _                    _______ ______ __  __ _____   _____
/ ____ | |      | | (_)   | | (_)                  |__   __|  ____|  \/  |  __ \ / ____|
| (___ | |_ __ _| |_ _ ___| |_ _  __ _ _   _  ___     | |  | |__  | \  / | |__) | (___
\___ \| __/ _` | __| / __| __| |/ _` | | | |/ _ \     | |  |  __| | |\/| |  ___/ \___ \
____) | || (_| | |_| \__ \ |_| | (_| | |_| |  __/     | |  | |____| |  | | |     ____) |
|_____/ \__\__,_|\__|_|___/\__|_|\__, |\__,_|\___|    |_|  |______|_|  |_|_|    |_____/
                                  | |
                                  |_|
*/



//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_calendrier_stats_clicked(const QDate &date){

    if( ui->checkBox_journeeStats->isChecked () | ui->checkBox_periodeStats->isChecked () ){

        //Si journ�e activ�e
        if( ui->checkBox_journeeStats->isChecked () )
            ui->lineEdit_dateDebutStats->setText ( date.toString ("dd/MM/yyyy") );

        //Si p�riode activ�e
        else if( ui->checkBox_periodeStats->isChecked () ){

            //Si aucun clique n'a �t� fait
            if( clickID == 0 ){

                ui->lineEdit_dateDebutStats->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 1;
            }
            //Lorsqu'un clique a d�j� �t� fait
            else if( clickID == 1 ){

                ui->lineEdit_dateFinStats->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 0;
            }
        }
    }
    else
        debugger ("S�lection crit�re","Veuillez s�lectionner une journ�e ou une p�riode avant de s�lectionner une date");
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur s�lectionne la checkBox "journ�e" pour sa recherche
  */
void MainWindow::on_checkBox_journeeStats_clicked(bool checked){

    ui->checkBox_periodeStats->setVisible ( !checked );
    ui->checkBox_periodeStats->setChecked ( false );

    ui->lineEdit_dateFinStats->setVisible ( !checked );

    ui->label_auStats->setVisible ( !checked );
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Importer"
  */
void MainWindow::on_btn_importerStats_clicked(){

    if( ui->checkBox_journeeStats->isChecked () || ui->checkBox_periodeStats->isChecked () ){

        if( ui->checkBox_journeeStats->isChecked () && ui->checkBox_periodeStats->isChecked () )
            debugger ("Double s�lection","Veuillez s�lectionner soit sur une journ�e soit sur une p�riode");
        else{

            if( ui->checkBox_journeeStats->isChecked () && ui->lineEdit_dateDebutStats->text ().isEmpty () )
                debugger ("S�lection date", "Veuillez s�lectionner une date dans votre crit�re de recherche par journ�e");

            else if( ui->checkBox_periodeStats->isChecked () && (ui->lineEdit_dateDebutStats->text ().isEmpty ()
                                                                 || ui->lineEdit_dateFinStats->text ().isEmpty () ) )
                     debugger ("S�lection date","Veuillez s�lectionner deux dates pour votre crit�re de recherche par p�riode");

            else{

                ui->btn_exporterStats->setVisible ( true );

                if( ui->radioButton_apiAutoRecherche->isChecked () || ui->radioButton_apiManuelleRecherche->isChecked ()
                        || ui->radioButton_apiAutoReconciliation->isChecked () || ui->radioButton_apiManuelleReconciliation->isChecked () ){

                    QDate dateDebut;
                    QDate dateFin;

                    QString dateD = dateDebut.fromString (ui->lineEdit_dateDebutStats->text (), "dd/MM/yyyy").toString ("yyyyMMdd");
                    QString dateF = dateFin.fromString (ui->lineEdit_dateFinStats->text (), "dd/MM/yyyy").toString ("yyyyMMdd");

                    if( ui->radioButton_apiAutoRecherche->isChecked () ){    //API auto -> recherche

                        contexte = "API automatique : Recherche";

                        if( ui->checkBox_journeeStats->isChecked () )
                            genererStats( dateD, "auto", "recherche");

                        else if ( ui->checkBox_periodeStats->isChecked () )
                            genererStats( dateD, dateF, "auto", "recherche");

                    }
                    else if ( ui->radioButton_apiManuelleRecherche->isChecked () ){    //API manuelle -> recherche

                        contexte = "API manuelle : Recherche";

                        if( ui->checkBox_journeeStats->isChecked () )
                            genererStats( dateD, "manuelle", "recherche");

                        else if ( ui->checkBox_periodeStats->isChecked () )
                            genererStats(dateD , dateF, "manuelle", "recherche");
                    }
                    else if( ui->radioButton_apiAutoReconciliation->isChecked () ){    //API auto -> envoiFichier

                        contexte = "API automatique : envoi de fichier";

                        if( ui->checkBox_journeeStats->isChecked () )
                            genererStats( dateD, "auto", "envoiFichier");

                        else if ( ui->checkBox_periodeStats->isChecked () )
                            genererStats(dateD, dateF, "auto", "envoiFichier");
                    }
                    else if( ui->radioButton_apiManuelleReconciliation->isChecked () ){    //API manuelle -> envoiFichier

                        contexte = "API manuelle : envoi de fichier";

                        if( ui->checkBox_journeeStats->isChecked () )
                            genererStats( dateD, "manuelle", "envoiFichier");

                        else if ( ui->checkBox_periodeStats->isChecked () )
                            genererStats( dateD, dateF, "manuelle", "envoiFichier");
                    }
                }
                else
                    debugger ("S�lection API","Veuillez cocher le type d'API pour g�n�rer vos statistiques");
            }
        }
    }
    else
        debugger ("S�lection journ�e/p�riode","Veuillez s�lectionner une votre crit�re de recherche [Journ�e/P�riode]");
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de g�n�rer les statistiques d'une API [typeAPI] sur une journ�e donn�e [date]
  */
void MainWindow::genererStats(QString date, QString typeAPI, QString contexte){

    Dial_bdd *bdd = new Dial_bdd( "\\\\" % config->getAdrIP ( ui->comboBox_VM_stats->currentText () )
                                  % "\\" % config->getValue (PATH_FICHIER_CONFIG, "Path", "apiManuelle") % "\\Base_Stats");

    QVector<double> stats = bdd->importerStats ( contexte, typeAPI, date );

    afficherGraphPoint ( stats );
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de g�n�rer les statistiques d'une API [typeAPI] sur une p�riode donn�e [dateDebut/DateFin]
  */
void MainWindow::genererStats(QString dateDebut, QString dateFin, QString typeAPI, QString contexte){

    Dial_bdd *bdd = new Dial_bdd( "\\\\" % config->getAdrIP ( ui->comboBox_VM_stats->currentText () )
                                  % "\\" % config->getValue (PATH_FICHIER_CONFIG, "Path", "apiManuelle") % "\\Base_Stats" );

    QVector<double> stats = bdd->importerStats ( contexte, typeAPI, dateDebut, dateFin );

    afficherGraphPoint ( stats );
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher les donn�es sur le graphique
  */
void MainWindow::afficherGraphPoint(QVector<double> vecteurY){

    QVector<double> x( vecteurY.size () );

    moyenne = 0;

    for (int i = 0; i < vecteurY.size () ; i++){

        moyenne += vecteurY.at (i);
        x[i] = i+1;
    }

    moyenne = moyenne / vecteurY.size ();
    ui->lineEdit_moyenne->setText ( QString::number ( moyenne ) % "s");


    int lastValue = vecteurY.at ( vecteurY.size () - 1 );
    int maxValue = lastValue;

    for(int j = 0 ; j < vecteurY.size () ; j++){

        if( maxValue < vecteurY.at (j) )
            maxValue = vecteurY.at (j);
    }


    // create graph and assign data to it:
    ui->widget_stats->addGraph();
    ui->widget_stats->graph(0)->setData(x, vecteurY);

    //Legend
    ui->widget_stats->xAxis->setLabel("Nombre de r�conciliation");
    ui->widget_stats->yAxis->setLabel("Temps de r�conciliation");

    // set axes ranges, so we see all data:
    ui->widget_stats->xAxis->setRange(0, vecteurY.size ());
    ui->widget_stats->yAxis->setRange(0, maxValue + 5);
    ui->widget_stats->replot();
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'exporter les statistiques
  */
void MainWindow::on_btn_exporterStats_clicked(){

    QString hostName = ui->comboBox_VM_stats->currentText ();
    QString nameFichierExport = QCoreApplication::applicationDirPath () % "/export_" % hostName % ".pdf" ;

    if( ui->widget_stats->savePdf ( nameFichierExport,1000,500) ){

        //Si journ�e
        if( ui->checkBox_journeeStats->isChecked () ){

            system("C:\\Postie\\postie.exe -host:mail.4axes.fr -from:stats@4axes.fr -to:statsvms@4axes.fr -s:\"Statistiques de "
                               + hostName.toLocal8Bit () + " - " + contexte.toLocal8Bit () + " du "
                               + ui->lineEdit_dateDebutStats->text ().toLocal8Bit () + "\" -msg:\"Moyenne (seconde) : \"" + (QString::number ( moyenne )).toLocal8Bit ()
                               + " -a:" + "\"" + nameFichierExport.toLocal8Bit () + "\"");
        }
        else if ( ui->checkBox_periodeStats->isChecked () ){        //Si p�riode

            system("C:\\Postie\\postie.exe -host:mail.4axes.fr -from:stats@4axes.fr -to:statsvms@4axes.fr -s:\"Statistiques de "
                   + hostName.toLocal8Bit () + " - " + contexte.toLocal8Bit () + " du "
                   + ui->lineEdit_dateDebutStats->text ().toLocal8Bit () + " au "
                   + ui->lineEdit_dateFinStats->text ().toLocal8Bit () + "\" -msg:\"Moyenne (seconde) : \"" + (QString::number ( moyenne )).toLocal8Bit ()
                   + " -a:" + "\"" + nameFichierExport.toLocal8Bit () + "\"");
        }
        QMessageBox::information (NULL,"Export statistique", "Statistiques export�es et envoy�es pour contr�le");
    }
    else
        debugger ("Erreur exportation statistique","Impossible d'exporter les statistiques");
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  La variable clickID est utilis�e par d'autre SLOT/m�thode sur diff�rent onglet
  L'astuce ici consiste � r�initialiser cette variable lorsque l'utilisateur change d'onglet
  */
void MainWindow::on_tabWidget_currentChanged(int index){

    clickID = 0;
}

