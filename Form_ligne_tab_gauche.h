#ifndef FORM_LIGNE_TAB_GAUCHE_H
#define FORM_LIGNE_TAB_GAUCHE_H

#include "Configuration.h"

#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

namespace Ui {
class Form_ligne_tab_gauche;
}

class Form_ligne_tab_gauche : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_ligne_tab_gauche(QString new_VM, QWidget *parent = 0);
    ~Form_ligne_tab_gauche();

    void initialiser();
    void ajouterLigne(QString ignorer, QString numFax, QString rnm, QString decoupage, QString provider, QString gs32);
    
private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();

private:
    Ui::Form_ligne_tab_gauche *ui;

    QString VM;

};

#endif // FORM_LIGNE_TAB_GAUCHE_H
