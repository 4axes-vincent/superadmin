#ifndef EXPORTATEURMAPPING_H
#define EXPORTATEURMAPPING_H

#include <Configuration.h>
#include <QTableWidget>

#include <QStringList>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QMessageBox>

class exportateurMapping
{
public:
    exportateurMapping();

    void exporterToOut(QString VM, QString donnees, QString new_ligne, QString old_ligne, QString contexte);
    void exporterToOut(QString VM, QStringList listInfo);
    void exporterOutToIN(QString VM);

private:


};

#endif // EXPORTATEURMAPPING_H
