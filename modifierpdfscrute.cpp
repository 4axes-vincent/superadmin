#include "modifierpdfscrute.h"
#include "ui_modifierpdfscrute.h"

modifierPDFScrute::modifierPDFScrute(QString VM, QString mutuelle, QString activite, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::modifierPDFScrute)
{
    ui->setupUi(this);
    this->setFixedSize (222,201);
    this->setWindowTitle ("Modifier information PDFScrute " % VM);

    ui->lineEdit_mutuelle->setText ( mutuelle);

    if( activite == "Active" )
        ui->radioButton_active->setChecked ( true );
    else
        ui->radioButton_inactive->setChecked ( true );

    machine = VM;

    ui->lineEdit_mutuelle->setReadOnly ( true );
}

modifierPDFScrute::~modifierPDFScrute()
{
    delete ui;
}













//----------------------------------------------------------------------------------------------------------------------
void modifierPDFScrute::debugger(QString titre, QString information){

    QMessageBox::warning (this,titre, information);
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de supprimer directement une donn�e dans le PDFScrute (action basculer)
  */
QPair<bool, QString> modifierPDFScrute::supprimerDonnees(QString nomMutuelle, QString VM){

    QPair<bool, QString> pairDonnee;

    //Contr�le
    if( !nomMutuelle.isEmpty () ){

        Configuration *configuration;

        QString pathPDFScrute = configuration->getPathPDFScrute ();

        if( VM == "4AXES_35" )
            pathPDFScrute.replace ("$","");

        QSettings settings("\\\\" % configuration->getAdrIP ( VM ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                           ,QSettings::IniFormat );

        settings.beginGroup ("MUTUELLE");

        //On r�cup�re l'activit� de la mutuelle puisqu'on souhaite basculer les donn�es avant suppression
        pairDonnee.second = settings.value ( nomMutuelle, "Incconu" ).toString ();

        settings.remove ( nomMutuelle );

        if( !settings.contains ( nomMutuelle ) )
            pairDonnee.first = true;
        else
            pairDonnee.first = false;

        settings.endGroup ();
    }
    else
        debugger ("Mutuelle","Veuillez s�lectionner une mutuelle");

    //retour
    return( pairDonnee );
}











//----------------------------------------------------------------------------------------------------------------------
void modifierPDFScrute::on_btn_annuler_clicked(){

    close();
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur valide son changement
  */
void modifierPDFScrute::on_btn_valider_clicked(){

    if( !ui->lineEdit_mutuelle->text ().isEmpty () ){

        if( ui->radioButton_active->isChecked () || ui->radioButton_inactive->isChecked () ){

            QString pathPDFScrute = config->getPathPDFScrute ();

            if( machine == "4AXES_35" )
                pathPDFScrute.replace ("$","");

            QSettings settings( "\\\\" % config->getAdrIP ( machine ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                                ,QSettings::IniFormat );

            settings.beginGroup ("MUTUELLE");

            QString activite;

            if( ui->radioButton_active->isChecked () )
                activite = "1";
            else
                activite = "0";

            settings.setValue ( ui->lineEdit_mutuelle->text (), activite);

            QMessageBox::information (NULL,"Modification","Valeur du PDFScrute.ini modif�e");

            emit succesEdit ( activite );

            close();
        }
        else
            debugger ("Activit� mutuelle","Veuillez s�lectionner l'activit� d'une mutuelle !");

    }
    else
        debugger ("Nom mutuelle","Veuillez saisir un nom de mutuelle !");
}




