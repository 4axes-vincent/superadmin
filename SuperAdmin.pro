#-------------------------------------------------
#
# Project created by QtCreator 2013-05-24T14:06:49
#
#-------------------------------------------------

QT       += core gui sql

TARGET = SuperAdmin
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Ajouter_VM.cpp \
    Configuration.cpp \
    Form_add_ligne.cpp \
    Form_set_ligne.cpp \
    Parametre.cpp \
    ImportateurMapping.cpp \
    ExportateurMapping.cpp \
    Trieur.cpp \
    ajouterpdfscrute.cpp \
    modifierpdfscrute.cpp \
    modifierpassedroit.cpp \
    dial_bdd.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    Ajouter_VM.h \
    Configuration.h \
    Form_add_ligne.h \
    Form_set_ligne.h \
    Parametre.h \
    ImportateurMapping.h \
    ExportateurMapping.h \
    Trieur.h \
    ajouterpdfscrute.h \
    modifierpdfscrute.h \
    modifierpassedroit.h \
    dial_bdd.h \
    qcustomplot.h

FORMS    += mainwindow.ui \
    Ajouter_VM.ui \
    Form_add_ligne.ui \
    Form_set_ligne.ui \
    Parametre.ui \
    ajouterpdfscrute.ui \
    modifierpdfscrute.ui \
    modifierpassedroit.ui


RC_FILE = ressource.rc

