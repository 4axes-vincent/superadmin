/********************************************************************************
** Form generated from reading UI file 'Form_add_ligne.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ADD_LIGNE_H
#define UI_FORM_ADD_LIGNE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_add_ligne
{
public:
    QLineEdit *lineEdit_numFax;
    QLineEdit *lineEdit_rnm;
    QLineEdit *lineEdit_provider;
    QCheckBox *checkBox_ignorer;
    QCheckBox *checkBox_DLL;
    QCheckBox *checkBox_decoupage;
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *labelnumFax;
    QLabel *label_nrm_oc;
    QLabel *label_provider;

    void setupUi(QWidget *Form_add_ligne)
    {
        if (Form_add_ligne->objectName().isEmpty())
            Form_add_ligne->setObjectName(QString::fromUtf8("Form_add_ligne"));
        Form_add_ligne->resize(1070, 153);
        lineEdit_numFax = new QLineEdit(Form_add_ligne);
        lineEdit_numFax->setObjectName(QString::fromUtf8("lineEdit_numFax"));
        lineEdit_numFax->setGeometry(QRect(110, 50, 161, 20));
        lineEdit_rnm = new QLineEdit(Form_add_ligne);
        lineEdit_rnm->setObjectName(QString::fromUtf8("lineEdit_rnm"));
        lineEdit_rnm->setGeometry(QRect(310, 50, 191, 20));
        lineEdit_provider = new QLineEdit(Form_add_ligne);
        lineEdit_provider->setObjectName(QString::fromUtf8("lineEdit_provider"));
        lineEdit_provider->setGeometry(QRect(690, 50, 231, 20));
        checkBox_ignorer = new QCheckBox(Form_add_ligne);
        checkBox_ignorer->setObjectName(QString::fromUtf8("checkBox_ignorer"));
        checkBox_ignorer->setGeometry(QRect(30, 50, 70, 17));
        checkBox_DLL = new QCheckBox(Form_add_ligne);
        checkBox_DLL->setObjectName(QString::fromUtf8("checkBox_DLL"));
        checkBox_DLL->setGeometry(QRect(970, 50, 70, 17));
        checkBox_decoupage = new QCheckBox(Form_add_ligne);
        checkBox_decoupage->setObjectName(QString::fromUtf8("checkBox_decoupage"));
        checkBox_decoupage->setGeometry(QRect(550, 50, 101, 17));
        btn_valider = new QPushButton(Form_add_ligne);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(800, 120, 75, 23));
        btn_annuler = new QPushButton(Form_add_ligne);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(900, 120, 75, 23));
        labelnumFax = new QLabel(Form_add_ligne);
        labelnumFax->setObjectName(QString::fromUtf8("labelnumFax"));
        labelnumFax->setGeometry(QRect(150, 20, 81, 16));
        label_nrm_oc = new QLabel(Form_add_ligne);
        label_nrm_oc->setObjectName(QString::fromUtf8("label_nrm_oc"));
        label_nrm_oc->setGeometry(QRect(370, 20, 51, 16));
        label_provider = new QLabel(Form_add_ligne);
        label_provider->setObjectName(QString::fromUtf8("label_provider"));
        label_provider->setGeometry(QRect(760, 20, 46, 13));

        retranslateUi(Form_add_ligne);

        QMetaObject::connectSlotsByName(Form_add_ligne);
    } // setupUi

    void retranslateUi(QWidget *Form_add_ligne)
    {
        Form_add_ligne->setWindowTitle(QApplication::translate("Form_add_ligne", "Form", 0, QApplication::UnicodeUTF8));
        checkBox_ignorer->setText(QApplication::translate("Form_add_ligne", "Ignorer", 0, QApplication::UnicodeUTF8));
        checkBox_DLL->setText(QApplication::translate("Form_add_ligne", "gs32", 0, QApplication::UnicodeUTF8));
        checkBox_decoupage->setText(QApplication::translate("Form_add_ligne", "D\303\251coupage", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Form_add_ligne", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("Form_add_ligne", "Annuler", 0, QApplication::UnicodeUTF8));
        labelnumFax->setText(QApplication::translate("Form_add_ligne", "Num\303\251ro de fax", 0, QApplication::UnicodeUTF8));
        label_nrm_oc->setText(QApplication::translate("Form_add_ligne", "RNM_OC", 0, QApplication::UnicodeUTF8));
        label_provider->setText(QApplication::translate("Form_add_ligne", "Provider", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_add_ligne: public Ui_Form_add_ligne {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ADD_LIGNE_H
