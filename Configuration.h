#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QStringList>
#include <QString>
#include <QSettings>
#include <QCoreApplication>
#include <QMessageBox>

class Configuration
{
public:
    Configuration();

    QString getPathMapping();
    QString getPathPDFScrute();
    QString getAdrIP(QString numVM);

    QString getPathBackup();
    QString getPathPoper();

    QString getValue(QString fichierIni, QString groupe, QString cle);
    QStringList getListeCle(QString fichierIni, QString groupe);

};

#endif // CONFIGURATION_H
