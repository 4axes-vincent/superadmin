#ifndef FORM_SET_LIGNE_H
#define FORM_SET_LIGNE_H

#include "Configuration.h"
#include "ExportateurMapping.h"

#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QTableWidget>

namespace Ui {
class Form_set_ligne;
}

class Form_set_ligne : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_set_ligne(QString new_VM, QString new_ignorer, QString new_numFax, QString new_rnm, QString new_decoupage, QString new_provider, QString new_dll, QWidget *parent = 0);
    ~Form_set_ligne();

    void initaliser();
    void modifierLigne(QString ancienneLigne, QString nouvelleLigne);
    
private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();


private:
    Ui::Form_set_ligne *ui;

    QString VM;
    QString ignorer;
    QString numFax;
    QString rnm;
    QString decoupage;
    QString provider;
    QString dll;
    QString ligneRechercher;
    QString ligneModifier;

};

#endif // FORM_SET_LIGNE_H
