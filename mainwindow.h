#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ImportateurMapping.h"
#include "ExportateurMapping.h"

#include "Ajouter_VM.h"

#include "Form_add_ligne.h"
#include "Form_set_ligne.h"

#include "Parametre.h"

#include "ajouterpdfscrute.h"
#include "modifierpdfscrute.h"

#include "Trieur.h"

#include "modifierpassedroit.h"

#include "dial_bdd.h"

#include "qcustomplot.h"

#include <QMainWindow>
#include <QSettings>
#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QIcon>
#include <QTableWidgetItem>
#include <QCoreApplication>
#include <QTreeWidgetItem>
#include <QMap>
#include <QDirIterator>
#include <QListWidgetItem>
#include <QDesktopServices>
#include <QUrl>
#include <QStringBuilder>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initialiser();
    void listerVM();
    void remplirTableWidget_1(QTableWidget &table);

    void remplirTableWidget_2(QTableWidget &table);
    void decouperInformation_1();
    void decouperInformation_2();

    void addChild(QTreeWidgetItem *parent, QString niveau_un);
    void remplirTableau(QStringList listeDetail, QTableWidget *table);
    void remplirTableau(QList<QStringList> listeDetail, QTableWidget *table);

    void supprimerToutFichierOUT();
    void initialiserTableau(QTableWidget *table, int nbColumn, int nbRow, QStringList header);
    void creerDossierGoodNIN(QString path, QDir dossier, QString mutuelle);

    void creerDossierUnknownNIN(QString path, QDir dossier, QString mutuelle);
    void chargerDonneeIHM(QStringList liste);
    void creerFichier(QString nom_oc);

    QString getRNM(QString ligne);
    QString getNumFax(QString ligne);

    QList<QStringList> rechercherParRNM(QString rnm, int pos);
    QList<QStringList> rechercherParFax(QString fax, int pos);

    QStringList rechercheDansMapping(QStringList liste, QString info);

    bool nonIgnorer(QString ligne);
    void debugger(QString titre, QString message);
    void ajouterLignesIHM(QList<QStringList> liste, QTableWidget *table);

    void genererStats(QString date, QString typeAPI, QString contexte);
    void genererStats(QString dateDebut, QString dateFin, QString typeAPI, QString contexte);
    void afficherGraphPoint (QVector<double> vecteurY);

    void afficherRecherche(int indexRecherche, QString motCle);
    void afficherRecherche(int indexRecherche, QString dateDebut, QString dateFin);
    void afficherNombreResultat(QString nbResultat);

    
private slots:

    void ajouterLignePDFScrute_1(QString mutuelle, QString activite);

    void ajouterLignePDFScrute_2(QString mutuelle, QString activite);

    void modifierLignePDFScrute_1(QString activiteMutuelle);

    void modifierLignePDFScrute_2(QString activiteMutuelle);

    void modifierPassDroit(QString passeDroit);

    void on_actionQuitter_triggered();

    void on_btn_importer_1_clicked();

    void on_tableWidget_1_cellClicked(int row, int column);

    void on_actionAjouter_V_M_triggered();

    void on_btn_importer_2_clicked();

    void on_btn_add_from_clicked();

    void on_tableWidget_2_cellClicked(int row, int column);

    void on_btn_add_to_clicked();

    void on_btn_exporter_clicked();

    void on_btn_add_1_clicked();

    void on_btn_add_2_clicked();

    void on_btn_supprimer_clicked();

    void on_btn_supprimer_2_clicked();

    void on_btn_modifier_clicked();

    void on_btn_actualiser_clicked();

    void on_btn_modifier_2_clicked();

    void on_actionParam_tres_triggered();

    void on_btn_importer_pdfscrute1_clicked();

    void on_btn_importer_pdfscrute2_clicked();

    void on_btn_ajouter_clicked();

    void on_btn_supprimer_pdfscrute_1_clicked();

    void on_btn_supprimer_pdfscrute_2_clicked();

    void on_btn_modifier_pdfscrute_clicked();

    void on_btn_basculer_1_clicked();

    void on_btn_basculer_2_clicked();

    void on_btn_ajouter_2_clicked();

    void on_btn_modifier_pdfscrute_2_clicked();

    void on_btn_importer_allMapping_clicked();

    void on_lineEdit_recheche_textChanged(const QString &arg1);

    void on_btn_initialiser_clicked();

    void on_btn_lister_clicked();

    void on_table_OC_itemClicked(QTableWidgetItem *item);

    void on_btn_creerTEMP_clicked();

    void on_btn_aReconcilier_clicked();

    void on_btn_OK_MANUEL_clicked();

    void on_btn_API_clicked();

    void on_btn_NR_clicked();

    void on_btn_receptionne_clicked();

    void on_calendarWidget_clicked(const QDate &date);

    void on_radioButton_tout_clicked();

    void on_radioButton_journee_clicked();

    void on_btn_raz_clicked();

    void on_radioButton_periode_clicked();

    void on_lineEdit_rechercher_mapping_1_textChanged(const QString &arg1);

    void on_lineEdit_recherchePdfScrute_1_textChanged(const QString &arg1);

    void on_lineEdit_recherchePdfScrute_2_textChanged(const QString &arg1);

    void on_radioButton_active_1_clicked();

    void on_radioButton_inactive_1_clicked();

    void on_radioButton_RAZ_1_clicked();

    void on_radioButton_active_2_clicked();

    void on_radioButton_inactive_2_clicked();

    void on_radioButton_RAZ_2_clicked();

    void on_tableWidget_pdfScrute_1_cellClicked(int row, int column);

    void on_tableWidget_pdfScrute_2_cellClicked(int row, int column);

    void on_tableWidget_pdfScrute_1_itemClicked(QTableWidgetItem *item);

    void on_tableWidget_pdfScrute_2_itemClicked(QTableWidgetItem *item);

    void on_btn_importerPasseDroit_clicked();

    void on_tableWidget_passeDroit_itemClicked(QTableWidgetItem *item);

    void on_btn_modifierPasseDroit_clicked();

    void on_table_recherche_cellDoubleClicked(int row, int column);

    void on_calendarWidget_rchPopper_clicked(const QDate &date);

    void on_checkBox_rchJournee_clicked();

    void on_checkBox_rchPeriode_clicked();

    void on_btn_importerStatsPopper_clicked();

    void on_lineEdit_rechercherMutuelle_textChanged(const QString &arg1);

    void on_btn_importeBaseStats_clicked();

    void on_lineEdit_rechercheInfoReconciliation_textChanged(const QString &arg1);

    void on_calendrier_stats_clicked(const QDate &date);

    void on_checkBox_journeeStats_clicked(bool checked);

    void on_btn_importerStats_clicked();

    void on_btn_exporterStats_clicked();

    void on_checkBox_journeeReconciliation_clicked(bool checked);

    void on_checkBox_periodeReconciliation_clicked(bool checked);

    void on_calendrierReconciliation_clicked(const QDate &date);

    void on_tabWidget_currentChanged(int index);

    void on_btn_filtreDate_clicked();

private:
    Ui::MainWindow *ui;
    ImportateurMapping *new_importateurMapping;
    exportateurMapping *new_exportateurMapping;
    Configuration *config;

    QTableWidgetItem *itemSelect;

    QString ligne;

    QString VM_1;
    QString VM_2;

    QString nomOcSelectonne;
    QString itemSelectionne_TEMP;
    QDate dateSelectionnee;
    QDate dateSelectionnee_p1;
    QDate dateSelectionnee_p2;

    QStringList header;
    QStringList header_pdfScrute;
    QStringList header_table_recherche;
    QStringList header_table_oc_gd_uk;
    QStringList header_table_oc;
    QStringList header_oc_passe_droit;
    QStringList headerStatsPopper;
    QStringList headerRechercherRecon;

    QStringList listeInfoMapping_1;
    QStringList listeInfoMapping_2;

    QStringList listeInfo_decoupee_1;
    QStringList listeInfo_decoupee_2;

    QStringList listLigne;
    QStringList listeInfo;

    QList<QStringList> liste_detail_pdfscrute_1;
    QList<QStringList> liste_detail_pdfscrute_2;

    QList<QStringList> listeMapping;

    QString mutuelleSelectionnee_1;
    QString activiteMutuelle_1;

    QString mutuelleSelectionnee_2;
    QString activiteMutuelle_2;

    QString mutuelleSelectPasseDroit;
    QString passeDroitSelect;

    QList<QStringList> listeStats;
    QList<QStringList> listeInfosReconciliation;

    int rowSelected;
    int clickID;

    QString contexte;
    float moyenne;

};

#endif // MAINWINDOW_H
