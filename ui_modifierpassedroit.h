/********************************************************************************
** Form generated from reading UI file 'modifierpassedroit.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODIFIERPASSEDROIT_H
#define UI_MODIFIERPASSEDROIT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_modifierPasseDroit
{
public:
    QLineEdit *lineEdit_nomMutuelle;
    QLineEdit *lineEdit_passeDroit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *label_3;

    void setupUi(QWidget *modifierPasseDroit)
    {
        if (modifierPasseDroit->objectName().isEmpty())
            modifierPasseDroit->setObjectName(QString::fromUtf8("modifierPasseDroit"));
        modifierPasseDroit->resize(479, 177);
        lineEdit_nomMutuelle = new QLineEdit(modifierPasseDroit);
        lineEdit_nomMutuelle->setObjectName(QString::fromUtf8("lineEdit_nomMutuelle"));
        lineEdit_nomMutuelle->setGeometry(QRect(80, 50, 211, 20));
        lineEdit_passeDroit = new QLineEdit(modifierPasseDroit);
        lineEdit_passeDroit->setObjectName(QString::fromUtf8("lineEdit_passeDroit"));
        lineEdit_passeDroit->setGeometry(QRect(80, 100, 381, 20));
        label = new QLabel(modifierPasseDroit);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 50, 51, 16));
        label_2 = new QLabel(modifierPasseDroit);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 100, 61, 20));
        btn_valider = new QPushButton(modifierPasseDroit);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(160, 140, 75, 23));
        btn_annuler = new QPushButton(modifierPasseDroit);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(250, 140, 75, 23));
        label_3 = new QLabel(modifierPasseDroit);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(190, 10, 121, 16));

        retranslateUi(modifierPasseDroit);

        QMetaObject::connectSlotsByName(modifierPasseDroit);
    } // setupUi

    void retranslateUi(QWidget *modifierPasseDroit)
    {
        modifierPasseDroit->setWindowTitle(QApplication::translate("modifierPasseDroit", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("modifierPasseDroit", "Mutuelle :", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("modifierPasseDroit", "Passe droit :", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("modifierPasseDroit", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("modifierPasseDroit", "Annuler", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("modifierPasseDroit", "Modification passe droit ", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class modifierPasseDroit: public Ui_modifierPasseDroit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODIFIERPASSEDROIT_H
