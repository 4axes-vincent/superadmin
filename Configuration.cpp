#include "Configuration.h"

Configuration::Configuration(){

}



//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne l'adresse IP en fonction de la VM
  Param�tre d'entr�e :

  QString numVM = VM
  */
QString Configuration::getAdrIP(QString numVM){

    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
    QString adrIP = settings.value("VM/"+numVM,"Default value VM/machine").toString();
    return(adrIP);
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le path du mapping
  */
QString Configuration::getPathMapping(){


        QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
        QString mapping = settings.value("path/mapping","Default value path/mapping").toString();

        return(mapping);
}





//----------------------------------------------------------------------------------------------------------------------
/*
   Retourne le path du PDFScrute
   */
QString Configuration::getPathPDFScrute(){


        QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
        QString pdfScrute = settings.value("path/pdfScrute","Default value path/pdfScrute").toString();

        return(pdfScrute);
}



//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le path backup
  */
QString Configuration::getPathBackup(){


    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
    QString backup = settings.value("path/backup","Default value path/backup").toString();

    return(backup);
}







//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le path utilis� par l'API (Dossier GoodNIN seulement)
  */
QString Configuration::getPathPoper(){

    //repPoper
    QSettings settings(QCoreApplication::applicationDirPath()+"\\config.ini", QSettings::IniFormat);
    QString repPoper = settings.value("path/repPoper","Default value path/repPoper").toString();

    return(repPoper);
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la valeur d'une cl� [cle] appartenant � un groupe [groupe] contenu dans un fichier [fichierIni]
  */
QString Configuration::getValue(QString fichierIni, QString groupe, QString cle){

    //repPoper
    QSettings settings( fichierIni, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.value ( cle, "-1").toString () );
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne la liste des cl�s du groupe [groupe] contenu dans le fichier [fichierIni]
  */
QStringList Configuration::getListeCle(QString fichierIni, QString groupe){

    //Ouverture du fichier config.ini
    QSettings settings( fichierIni , QSettings::IniFormat);
    settings.beginGroup( groupe );
    return( settings.allKeys () );
}
