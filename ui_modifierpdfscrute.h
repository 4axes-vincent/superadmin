/********************************************************************************
** Form generated from reading UI file 'modifierpdfscrute.ui'
**
** Created: Mon 9. May 15:57:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODIFIERPDFSCRUTE_H
#define UI_MODIFIERPDFSCRUTE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_modifierPDFScrute
{
public:
    QRadioButton *radioButton_inactive;
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QLabel *label;
    QLabel *label_2;
    QRadioButton *radioButton_active;
    QLineEdit *lineEdit_mutuelle;

    void setupUi(QWidget *modifierPDFScrute)
    {
        if (modifierPDFScrute->objectName().isEmpty())
            modifierPDFScrute->setObjectName(QString::fromUtf8("modifierPDFScrute"));
        modifierPDFScrute->resize(222, 201);
        radioButton_inactive = new QRadioButton(modifierPDFScrute);
        radioButton_inactive->setObjectName(QString::fromUtf8("radioButton_inactive"));
        radioButton_inactive->setGeometry(QRect(130, 130, 82, 17));
        btn_valider = new QPushButton(modifierPDFScrute);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(20, 160, 75, 23));
        btn_annuler = new QPushButton(modifierPDFScrute);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(130, 160, 75, 23));
        label = new QLabel(modifierPDFScrute);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 20, 141, 16));
        label_2 = new QLabel(modifierPDFScrute);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(80, 60, 61, 16));
        radioButton_active = new QRadioButton(modifierPDFScrute);
        radioButton_active->setObjectName(QString::fromUtf8("radioButton_active"));
        radioButton_active->setGeometry(QRect(30, 130, 82, 17));
        lineEdit_mutuelle = new QLineEdit(modifierPDFScrute);
        lineEdit_mutuelle->setObjectName(QString::fromUtf8("lineEdit_mutuelle"));
        lineEdit_mutuelle->setGeometry(QRect(20, 90, 181, 20));

        retranslateUi(modifierPDFScrute);

        QMetaObject::connectSlotsByName(modifierPDFScrute);
    } // setupUi

    void retranslateUi(QWidget *modifierPDFScrute)
    {
        modifierPDFScrute->setWindowTitle(QApplication::translate("modifierPDFScrute", "Form", 0, QApplication::UnicodeUTF8));
        radioButton_inactive->setText(QApplication::translate("modifierPDFScrute", "Inactive", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("modifierPDFScrute", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("modifierPDFScrute", "Annuler", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("modifierPDFScrute", "Modifier donn\303\251es PDFScrute", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("modifierPDFScrute", "Mutuelle :", 0, QApplication::UnicodeUTF8));
        radioButton_active->setText(QApplication::translate("modifierPDFScrute", "Active", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class modifierPDFScrute: public Ui_modifierPDFScrute {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODIFIERPDFSCRUTE_H
