#include "ImportateurMapping.h"

ImportateurMapping::ImportateurMapping(){

}





//----------------------------------------------------------------------------------------------------------------------
QStringList ImportateurMapping::importerDonnee(QString VM){

    QStringList             listeInfo;


    if( ! VM.isEmpty() ){
        Configuration           *config          = new Configuration();

        QString                 ligneCourante;
        QString                 adrIP           = config->getAdrIP(VM);
//        QString                 path = config->getPathMapping() + "\\" + VM + "\\mapping.csv" ;
        QString                 path            = "\\\\" + adrIP + "\\" + config->getPathMapping() + "\\mapping.csv" ;



        //On backup le fichier d'origine au moment de l'importation
        backupFichier(VM, adrIP, config);
        backupFichier_OUT(VM, adrIP, config);

        //Ouverture du fichier � analyser
        QFile fichierEntree(path);

        if ( ! fichierEntree.open(QIODevice::ReadWrite | QIODevice::Text))
            QMessageBox::warning(NULL,"Probl�me ouverture fichier","ERREUR - Probleme ouverture fichier entree: mapping.csv");

        else{

            QTextStream fluxEntrant(&fichierEntree);

            //Tant qu'on est pas arriv� � la fin du fichier
            while( ! fluxEntrant.atEnd() ){

                ligneCourante = fluxEntrant.readLine();

                if( !ligneCourante.isEmpty() )
                    listeInfo.push_back(ligneCourante);
            }
        }

        fichierEntree.close();
    }

    return(listeInfo);
}







//----------------------------------------------------------------------------------------------------------------------
void ImportateurMapping::backupFichier(QString VM, QString adrIP, Configuration *config){

    QDateTime           dateTime;
    QFile fichierCSV;

//    QFile::copy(config->getPathPDFScrute() + "\\" + VM + "\\mapping.csv", config->getPathBackup() + "\\" + "mapping_"+ VM + "_" + dateTime.currentDateTime().toString("yyyyMMdd_hh_mm_ss").append(".csv"));
    if ( !fichierCSV.copy("\\\\" + adrIP + "\\" + config->getPathMapping()+ "\\mapping.csv",config->getPathBackup() + "\\" + "mapping_"+ VM + "_" + dateTime.currentDateTime().toString("yyyyMMdd_hh_mm_ss").append(".csv")) )
        QMessageBox::warning (NULL,"Copie fichier CSV","Impossble de copier le fichier mapping.csv de la " + VM + " -> " + fichierCSV.errorString () );
}





//----------------------------------------------------------------------------------------------------------------------
void ImportateurMapping::backupFichier_OUT(QString VM, QString adrIP, Configuration *config){


    QFile fichierOUT;

//    QFile::copy(config->getPathPDFScrute() + "\\" + VM + "\\mapping.csv", config->getPathBackup()+"\\mapping_" + VM + ".OUT");
    if ( !fichierOUT.copy("\\\\" + adrIP + "\\" + config->getPathMapping()+ "\\mapping.csv", config->getPathBackup()+"\\mapping_" + VM + ".OUT") )
        QMessageBox::warning (NULL,"Copie fichier OUT","Impossble de copier le fichier mapping.csv de la " + VM + " -> " + fichierOUT.errorString () );

}















