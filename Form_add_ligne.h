#ifndef FORM_ADD_LIGNE_H
#define FORM_ADD_LIGNE_H

#include "Configuration.h"
#include "ExportateurMapping.h"

#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QTableWidget>
#include <QDir>


namespace Ui {
class Form_add_ligne;
}

class Form_add_ligne : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_add_ligne(QString new_VM, QStringList liste, QWidget *parent = 0);
    ~Form_add_ligne();

    void initialiser();
    void ajouterLigne(QString ignorer, QString numFax, QString rnm, QString decoupage, QString provider, QString gs32);
    bool siDonneeExistante(QString numFax, QString rnm);

    void creerDossiersOC(QString rnm_oc);
    
private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();

private:
    Ui::Form_add_ligne *ui;

    QString VM;
    QStringList listeMapping;
};

#endif // FORM_ADD_LIGNE_H
