#ifndef TRIEUR_H
#define TRIEUR_H

#include <QSettings>
#include <QStringBuilder>
#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QIcon>
#include <QTableWidgetItem>
#include <QCoreApplication>
#include <QTreeWidgetItem>
#include <QMap>
#include <QDirIterator>
#include <QDate>
#include <QLineEdit>

class Trieur
{
public:
    Trieur(QString dir_GOOD, QString dir_UNKNOWN);

    QStringList trierErreur(QLineEdit *IHM_Ligne_Total);
    QStringList trierErreurDate(QDate date, QLineEdit *IHM_Ligne_Total);
    QStringList trierErreurPeriode(QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total);

    QStringList trierAPI(QLineEdit *IHM_Ligne_Total);
    QStringList trierAPIDate(QDate date, QLineEdit *IHM_Ligne_Total);
    QStringList trierAPIPeriode(QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total);

    QStringList trierNR(QLineEdit *IHM_Ligne_Total);
    QStringList trierNRDate(QDate date, QLineEdit *IHM_Ligne_Total);
    QStringList trierNRPeriode(QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total);

    QStringList trierOK_MANUEL(QLineEdit *IHM_Ligne_Total);
    QStringList trierOK_MANUELDate(QDate date, QLineEdit *IHM_Ligne_Total);
    QStringList trierOK_MANUELPeriode(QDate date_p1, QDate date_p2, QLineEdit *IHM_Ligne_Total);

    QStringList trierRecv(QLineEdit *IHM_Ligne_Total);
    QList<QStringList> trierErreur_admin ();
    void trierListe (QList<QStringList> &liste);

private:

    QString pathG;
    QString pathU;

};

#endif // TRIEUR_H
