#ifndef PARAMETRE_H
#define PARAMETRE_H

#include <QWidget>

namespace Ui {
class Parametre;
}

class Parametre : public QWidget
{
    Q_OBJECT
    
public:
    explicit Parametre(QWidget *parent = 0);
    ~Parametre();

    void initialiser();
    
private:
    Ui::Parametre *ui;
};

#endif // PARAMETRE_H
