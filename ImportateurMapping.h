#ifndef IMPORTATEURMAPPING_H
#define IMPORTATEURMAPPING_H

#include <Configuration.h>

#include <QWidget>
#include <QSettings>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QIcon>
#include <QTableWidgetItem>
#include <QCoreApplication>
#include <QDateTime>
#include <QString>

class ImportateurMapping
{
public:
    ImportateurMapping();

    QStringList importerDonnee(QString VM);
    void backupFichier(QString VM, QString adrIP, Configuration *config);
    void backupFichier_OUT(QString VM, QString adrIP, Configuration *config);

private:

    QStringList listLigne;
};

#endif // IMPORTATEURMAPPING_H
