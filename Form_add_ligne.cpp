#include "Form_add_ligne.h"
#include "ui_Form_add_ligne.h"

Form_add_ligne::Form_add_ligne(QString new_VM, QStringList liste, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_add_ligne)
{
    ui->setupUi(this);
    VM = new_VM;
    listeMapping = liste;
    initialiser();
}







//----------------------------------------------------------------------------------------------------------------------
Form_add_ligne::~Form_add_ligne(){
    delete ui;
}






//----------------------------------------------------------------------------------------------------------------------
/*
  Initialise la fen�tre d'ajout
  */
void Form_add_ligne::initialiser(){

    this->setWindowTitle("Nouvelle ligne");
    this->setFixedSize(1070,153);
}






//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ajouter une ligne dans le mapping apr�s validation
  */
void Form_add_ligne::ajouterLigne(QString ignorer, QString numFax, QString rnm, QString decoupage, QString provider, QString gs32){

    //Cr�ation d'un exporteur de mapping
    exportateurMapping *new_exporter = new exportateurMapping();

    //On met le rnm en majuscule
    QString nomOc = rnm.mid ( rnm.indexOf ("_") + 1, rnm.length () );
    QString numOc = rnm.mid ( 0, rnm.indexOf ("_") );
    rnm = numOc + "_" + nomOc.toUpper ();

    //Cr�ation d'une ligne de donn�e
    QString data = ignorer + numFax + ";" + rnm + ";" + decoupage + ";" + provider + ";" + gs32;

    //Exportation vers le fichier OUT
    new_exporter->exporterToOut(VM, data, NULL, NULL, "ajouter");

    //Cr�ation du dossier de l'OC
    creerDossiersOC(rnm);

    close();

}









//----------------------------------------------------------------------------------------------------------------------
void Form_add_ligne::on_btn_annuler_clicked(){

    close();
}









//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : lorsque l'utilisateur valide son choix
  */
void Form_add_ligne::on_btn_valider_clicked(){


    QString ignorer             = "";
    QString decoupage           = "";
    QString gs32                = "";

    QString numFax              = ui->lineEdit_numFax->text();
    QString rnm                 = ui->lineEdit_rnm->text();
    QString provider            = ui->lineEdit_provider->text();

    if( !numFax.isEmpty() && !rnm.isEmpty() ){

        if( ui->checkBox_ignorer->isChecked() )
            ignorer = ";";

        if( ui->checkBox_decoupage->isChecked() )
            decoupage = "*";

        if( ui->checkBox_DLL->isChecked() )
            gs32 = "gs32";

        if( siDonneeExistante(numFax,rnm) )
            QMessageBox::warning(NULL,"Doublons","L'association num�ro de fax et RNM existe d�j� !");
        else
            ajouterLigne(ignorer, numFax, rnm, decoupage, provider, gs32);
    }
    else
        QMessageBox::warning(NULL,"Information manquante","Veuillez remplir tous les champs !");

}













//----------------------------------------------------------------------------------------------------------------------
/*
  Test l'existance de l'association numFax (num�ro de fax) et RNM_OC dans la liste
  */
bool Form_add_ligne::siDonneeExistante (QString numFax, QString rnm){

    for(int i = 0 ; i < listeMapping.length () ; i++){

        if( (listeMapping.at (i) == numFax ) && listeMapping.at (i).contains (rnm))
            return(true);
    }
    return(false);
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Cr�ation des dossiers pour l'OC
  */
void Form_add_ligne::creerDossiersOC (QString rnm_oc){

    Configuration           *config             = new Configuration();

    QDir                    dossier;

    QString                 dossierArchive      = "ArchivesDPEC";
    QString                 dossierErreur       = "ERREUR";
    QString                 dossierOK           = "OK";
    QString                 dossierNR           = "NR";
    QString                 dossierOK_MANUEL    = "OK_MANUEL";

    QString                 adrIP               = "\\\\" + config->getAdrIP (VM);

    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc);
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierArchive);

    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur);
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierOK);
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur + "\\" + dossierNR);
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur + "\\" + dossierOK_MANUEL);

    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\");
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\" + dossierNR);
    dossier.mkdir (adrIP + "\\" + config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\" + dossierOK_MANUEL);


//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc);
//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierArchive);

//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur);
//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierOK);
//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur + "\\" + dossierNR);
//    dossier.mkdir (config->getPathPoper () + "\\GoodNIN" + "\\" + rnm_oc + "\\" + dossierErreur + "\\" + dossierOK_MANUEL);

//    dossier.mkdir (config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\");
//    dossier.mkdir (config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\" + dossierNR);
//    dossier.mkdir (config->getPathPoper () + "\\UnknownNIN" + "\\" + rnm_oc + "\\" + dossierOK_MANUEL);


}
