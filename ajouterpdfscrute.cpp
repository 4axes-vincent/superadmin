#include "ajouterpdfscrute.h"
#include "ui_ajouterpdfscrute.h"

ajouterPDFScrute::ajouterPDFScrute(QString VM, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ajouterPDFScrute)
{
    ui->setupUi(this);

    this->setWindowTitle ("Ajouter donn�es PDFScrute : " % VM);

    this->setFixedSize (222,201);

    machine = VM;
}

ajouterPDFScrute::~ajouterPDFScrute()
{
    delete ui;
}





//----------------------------------------------------------------------------------------------------------------------
void ajouterPDFScrute::debugger(QString titre, QString information){

    QMessageBox::warning (this,titre, information);
}















//----------------------------------------------------------------------------------------------------------------------
void ajouterPDFScrute::ajouterDonnee(QString nomMutuelle, QString activite, QString VM){

    Configuration *configuration;

    QString pathPDFScrute = configuration->getPathPDFScrute ();

    if( VM == "4AXES_35" )
        pathPDFScrute.replace ("$","");

    QSettings settings("\\\\" % configuration->getAdrIP ( VM ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                       ,QSettings::IniFormat);
    settings.beginGroup ("MUTUELLE");

    settings.setValue (nomMutuelle, activite);

    //On v�rifie que la cl� existe bien
    if( settings.contains ( nomMutuelle ) )
        QMessageBox::information (NULL,"Transfert","Transfert donn�es effectu�");
    else
        QMessageBox::information (NULL,"Transfert","Erreur lors du transfert de donn�es");

    settings.endGroup ();
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur valide son ajout
  */
void ajouterPDFScrute::on_btn_valider_clicked(){

    if( !ui->lineEdit_mutuelle->text ().isEmpty () ){

        QString nomMutuelle = ui->lineEdit_mutuelle->text ();

        if( ui->radioButton_active->isChecked () || ui->radioButton_inactive->isChecked () ){

            QString activite;

            QString pathPDFScrute = config->getPathPDFScrute ();

            if( machine == "4AXES_35" )
                pathPDFScrute.replace ("$","");

            QSettings settings("\\\\" % config->getAdrIP ( machine ) % "\\" % pathPDFScrute % "\\PDFScrute.ini"
                               ,QSettings::IniFormat);
            settings.beginGroup ("MUTUELLE");

            if( ui->radioButton_active->isChecked () )
                activite = "1";
            else
                activite = "0";

           settings.setValue ( nomMutuelle, activite);


            if( settings.contains ( nomMutuelle ) ){

                settings.endGroup ();
                QMessageBox::information (NULL,"Confirmation", "La mutuelle a �t� ajout�");


                settings.beginGroup ("Spec");
                settings.setValue ( nomMutuelle, nomMutuelle.mid (0, nomMutuelle.indexOf ("_") ) + "-" );
                settings.endGroup ();

                emit succesInsert (nomMutuelle, activite);
            }
            else
                debugger ("Erreur ajout donn�es","Impossibe d'ins�rer une valeur dans le PDFScrute");



            close();
        }
        else
            debugger ("Activit� mutuelle","Veuillez s�lectionner l'activit� de la mutuelle");
    }
    else
        debugger ("Nom mutuelle","Veuillez saisir un nom de mutuelle !");
}




//----------------------------------------------------------------------------------------------------------------------
/*
  Fermeture
  */
void ajouterPDFScrute::on_btn_annuler_clicked(){

    close();
}
