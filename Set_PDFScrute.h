#ifndef SET_PDFSCRUTE_H
#define SET_PDFSCRUTE_H

#include "Configuration.h"
#include "ExportateurPdfScrute.h"

#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>


namespace Ui {
class Set_PDFScrute;
}

class Set_PDFScrute : public QWidget
{
    Q_OBJECT
    
public:
    explicit Set_PDFScrute(QWidget *parent);
    ~Set_PDFScrute();

    
private slots:



private:
    Ui::Set_PDFScrute *ui;
    Configuration *config;

};

#endif // SET_PDFSCRUTE_H
