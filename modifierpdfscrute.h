#ifndef MODIFIERPDFSCRUTE_H
#define MODIFIERPDFSCRUTE_H

#include "Configuration.h"

#include <QWidget>
#include <QSettings>
#include <QStringBuilder>
#include <QMessageBox>

namespace Ui {
class modifierPDFScrute;
}

class modifierPDFScrute : public QWidget
{
    Q_OBJECT
    
public:
    explicit modifierPDFScrute(QString VM, QString mutuelle, QString activite, QWidget *parent = 0);
    ~modifierPDFScrute();

    void debugger(QString titre, QString information);

    QPair<bool, QString> supprimerDonnees(QString mutuelle, QString VM);
    
private slots:
    void on_btn_annuler_clicked();

    void on_btn_valider_clicked();

signals:

    void succesEdit(QString activite);

private:
    Ui::modifierPDFScrute *ui;
    Configuration *config;

    QString machine;
    QString activiteVM;
    QString nomMutuelle;
};

#endif // MODIFIERPDFSCRUTE_H
