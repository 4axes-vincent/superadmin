/********************************************************************************
** Form generated from reading UI file 'Set_PDFScrute.ui'
**
** Created: Mon 28. Dec 13:31:14 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SET_PDFSCRUTE_H
#define UI_SET_PDFSCRUTE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Set_PDFScrute
{
public:

    void setupUi(QWidget *Set_PDFScrute)
    {
        if (Set_PDFScrute->objectName().isEmpty())
            Set_PDFScrute->setObjectName(QString::fromUtf8("Set_PDFScrute"));
        Set_PDFScrute->resize(301, 249);

        retranslateUi(Set_PDFScrute);

        QMetaObject::connectSlotsByName(Set_PDFScrute);
    } // setupUi

    void retranslateUi(QWidget *Set_PDFScrute)
    {
        Set_PDFScrute->setWindowTitle(QApplication::translate("Set_PDFScrute", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Set_PDFScrute: public Ui_Set_PDFScrute {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SET_PDFSCRUTE_H
