#include "Form_set_ligne.h"
#include "ui_Form_set_ligne.h"

Form_set_ligne::Form_set_ligne(QString new_VM,QString new_ignorer, QString new_numFax, QString new_rnm, QString new_decoupage, QString new_provider, QString new_dll, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_set_ligne)
{
    ui->setupUi(this);

    VM                  = new_VM;
    ignorer             = new_ignorer;
    numFax              = new_numFax;
    rnm                 = new_rnm;
    decoupage           = new_decoupage;
    provider            = new_provider;
    dll                 = new_dll;

    initaliser();
}












//----------------------------------------------------------------------------------------------------------------------
Form_set_ligne::~Form_set_ligne(){

    delete ui;
}













//----------------------------------------------------------------------------------------------------------------------
void Form_set_ligne::initaliser(){


    this->setWindowTitle("Modification ligne");
    this->setFixedSize(1070,153);


    if( !decoupage.isEmpty() )
        ui->checkBox_decoupage->setChecked(true);

    if( !dll.isEmpty() )
        ui->checkBox_DLL->setChecked(true);


    ui->lineEdit_numFax->setText(numFax);
    ui->lineEdit_rnm->setText(rnm);
    ui->lineEdit_provider->setText(provider);

    if( ignorer == "Oui" ){

        ui->checkBox_ignorer->setChecked(true);
        ligneRechercher = ";" + numFax + ";" + rnm + ";" + decoupage + ";" + provider + ";" + dll;
    }
    else
        ligneRechercher = numFax + ";" + rnm + ";" + decoupage + ";" + provider + ";" + dll;

}















//----------------------------------------------------------------------------------------------------------------------
void Form_set_ligne::on_btn_valider_clicked(){

    decoupage.clear();
    dll.clear();
    ignorer.clear();

    rnm = ui->lineEdit_rnm->text();
    numFax = ui->lineEdit_numFax->text();
    provider = ui->lineEdit_provider->text();

    if( !rnm.isEmpty() && !numFax.isEmpty() ){

        if( ui->checkBox_decoupage->isChecked() )
            decoupage = "*";

        if( ui->checkBox_DLL->isChecked() )
            dll = "gs32";

        if( ui->checkBox_ignorer->isChecked() )
            ligneModifier = ";" + numFax + ";" + rnm + ";" + decoupage + ";" + provider + ";" + dll;

        else
            ligneModifier = numFax + ";" + rnm + ";" + decoupage + ";" + provider + ";" + dll;

        modifierLigne(ligneRechercher, ligneModifier);
    }
    else
        QMessageBox::warning(NULL,"Champs vide","Veuillez remplir tous les champ");

}











//----------------------------------------------------------------------------------------------------------------------
void Form_set_ligne::modifierLigne(QString ancienneLigne, QString nouvelleLigne){

    exportateurMapping *new_exporter = new exportateurMapping();
    new_exporter->exporterToOut(VM, NULL, nouvelleLigne, ancienneLigne, "modifier");
    close();
}
















//----------------------------------------------------------------------------------------------------------------------
void Form_set_ligne::on_btn_annuler_clicked(){

    close();
}
